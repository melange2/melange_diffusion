/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "experiment_settings.h"

ExperimentSettings::ExperimentSettings(Model *mod)
{
    model = mod;
}

ExperimentSettings::~ExperimentSettings()
{
    //dtor
}

void ExperimentSettings::SetScalingFactor(Real scalefactor)
{
    model->scale_factor = scalefactor;
   	for (int i=0; i<model->nb_of_particles; i++)
    {
        model->particle_list[i].scale_factor = scalefactor;
    }
}

void ExperimentSettings::ScaleSecNeigStrength(Real fac)
{	

	Particle *prtcl;

    // first the breakingstrength for springs, to the averaged general particle strength
    // --> then multiply it by the factor given in the function call
    for (int i = 0; i < model->nb_of_particles; i++)
    {
		prtcl = &(model->particle_list[i]);

        for (int j = 0; j < 6; j++)                            
        {
            if (prtcl->second_neighbour_list[j])
            {
				prtcl->second_neigh_break_Str[j] = (prtcl->br_strength + prtcl->second_neighbour_list[j]->br_strength) / 2.0;
				
				// this is the important bit
				prtcl->second_neigh_break_Str[j] *= fac;
			}
		}
    }
}

void ExperimentSettings::SetDensityForAllParticles(Real density)
{
    for (int i=0; i<model->nb_of_particles; i++)
    {
        model->particle_list[i].density = density;
    } 
}

void ExperimentSettings::SetDensity(Real density, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2)
{
	Real xmin = x1 < x2 ? x1 : x2;
	Real xmax = x1 < x2 ? x2 : x1;
	Real ymin = y1 < y2 ? y1 : y2;
	Real ymax = y1 < y2 ? y2 : y1;
	Real zmin = z1 < z2 ? z1 : z2;
	Real zmax = z1 < z2 ? z2 : z1;
	
    for (int i=0; i<model->nb_of_particles; i++)
    {
		if (model->particle_list[i].pos[0] > xmin && model->particle_list[i].pos[0] < xmax)
			if (model->particle_list[i].pos[1] > ymin && model->particle_list[i].pos[1] < ymax)
				if (model->particle_list[i].pos[2] > zmin && model->particle_list[i].pos[2] < zmax)
			        model->particle_list[i].density = density;
    }
}

void ExperimentSettings::SetStandardYoungsModulusAndV(Real real_young, Real v)
{
	model->standard_real_young = real_young;

	if (v >= 0.25 || v <= -1)
	{
		cout << endl << "!!! Poisson ratio is restricted to values beteen 0.25 and -1 !!! " << endl;
		cout << "!!! Exiting now !!!" << endl;
		cout << "!!! Please change the value !!!" << endl;
		cout << endl;
		
		exit(0);    
	}
	
    for (int i=0; i<model->nb_of_particles; i++)
    {
        model->particle_list[i].real_young = real_young;
        model->particle_list[i].standard_real_young = real_young;
        
        model->particle_list[i].SetYoungsModulusAndV(real_young, v);
    }
}

void ExperimentSettings::SetRealYoungAndV(Real real_young, Real v, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2)
{
	
	Particle *prtcl;
	
	Real xmin = x1 < x2 ? x1 : x2;
	Real xmax = x1 < x2 ? x2 : x1;
	Real ymin = y1 < y2 ? y1 : y2;
	Real ymax = y1 < y2 ? y2 : y1;
	Real zmin = z1 < z2 ? z1 : z2;
	Real zmax = z1 < z2 ? z2 : z1;
	
    for (int i=0; i<model->nb_of_particles; i++)
    {
		prtcl = &(model->particle_list[i]);
		
		if (prtcl->pos[0] > xmin && prtcl->pos[0] < xmax)
		{
			if (prtcl->pos[1] > ymin && prtcl->pos[1] < ymax)
			{
				if (prtcl->pos[2] > zmin && prtcl->pos[2] < zmax)
				{
					prtcl->SetYoungsModulusAndV(real_young, v);
				}
			}
		}
    }
}

void ExperimentSettings::SetTemperature( Real minx , Real maxx , Real miny , Real maxy , Real minz , Real maxz, Real temperature)
{
	Particle *prtcl;

    for (int i=0; i<model->nb_of_particles; i++)
    {
		prtcl = &(model->particle_list[i]);
		
		if (prtcl->pos[0] > minx && prtcl->pos[0] < maxx)
		{
			if (prtcl->pos[1] > miny && prtcl->pos[1] < maxy)
			{
				if (prtcl->pos[2] > minz && prtcl->pos[2] < maxz)
				{
					prtcl->temperature = temperature;
				}
			}
		}
	}
}

void ExperimentSettings::SetPowerLawViscosity( Real minx , Real maxx , Real miny , Real maxy , Real minz , Real maxz, 
	Real A, Real Ea, Real Va, Real n )
{

	Particle *prtcl;

    for (int i=0; i<model->nb_of_particles; i++)
    {
		prtcl = &(model->particle_list[i]);
		
		if (prtcl->pos[0] > minx && prtcl->pos[0] < maxx)
		{
			if (prtcl->pos[1] > miny && prtcl->pos[1] < maxy)
			{
				if (prtcl->pos[2] > minz && prtcl->pos[2] < maxz)
				{
					prtcl->power_law_viscosity = true;

					prtcl->Ea = Ea;
					prtcl->Va = Va; 
					prtcl->A = A;
					prtcl->n = n;
				}
			}
		}
	}
}

void ExperimentSettings::ActivateGravity()
{
    model->ActivateGravity();
}

void ExperimentSettings::SetViscosity(Real viscosity, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2)
{
	Particle *prtcl;
	
	Real xmin = x1 < x2 ? x1 : x2;
	Real xmax = x1 < x2 ? x2 : x1;
	Real ymin = y1 < y2 ? y1 : y2;
	Real ymax = y1 < y2 ? y2 : y1;
	Real zmin = z1 < z2 ? z1 : z2;
	Real zmax = z1 < z2 ? z2 : z1;
	
	// we have to reset the viscoelasticity-factor for every single particle	
	for (int i = 0; i < model->nb_of_particles; i++)
	{
		prtcl = &(model->particle_list[i]);
		
		if (prtcl->pos[0] > xmin && prtcl->pos[0] < xmax)
		{
			if (prtcl->pos[1] > ymin && prtcl->pos[1] < ymax)
			{
				if (prtcl->pos[2] > zmin && prtcl->pos[2] < zmax)
				{
					prtcl->viscosity = viscosity;
				}
			}
		}
	}
}

void ExperimentSettings::SetTimeStep(Real time)
{
	model->timestep = time;
}

void ExperimentSettings::ActivateLatticeViscoelasticity()
{
	model->viscoelastic = true;
	
	for (int i = 0; i < model->nb_of_particles; i++)
    {
		model->particle_list[i].viscoelastic = true;
	}
}

// removes springs in a certain volume, making the material granular while the rest of the model remains a lattice
void ExperimentSettings::RemoveSprings (Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax)
{	
	Particle *prtcl;
	int neigh = 0;

	for (int i = 0; i < model->nb_of_particles; i++)
	{
		prtcl = &(model->particle_list[i]);

		if (prtcl->pos[0] < xmax && prtcl->pos[0] > xmin)
		{
			if (prtcl->pos[1] < ymax && prtcl->pos[1] > ymin)
			{
				if (prtcl->pos[2] < zmax && prtcl->pos[2] > zmin)
				{
					for (int j = 0; j < 12; j++)                            
					{
						if (prtcl->neighbour_list[j])
						{
						
							for (int k = 0; k < 12; k++) 
							{
								if (prtcl->neighbour_list[j]->neighbour_list[k] == prtcl)
								{
									neigh = k;
									break;
								}
							}

							prtcl->neighbour_list[j]->neighbour_list[neigh] = NULL;      
							prtcl->neighbour_list[j]->break_Str[neigh] = 0.0;                
							prtcl->neighbour_list[j]->is_broken = true;							
							prtcl->neighbour_list[j]->repulsion = true;                              
							
							prtcl->neighbour_list[j] = NULL;
							prtcl->break_Str[j] = 0.0;
							
						}
					}
					for (int j = 0; j < 6; j++)                            
					{
						if (prtcl->second_neighbour_list[j])
						{
				
							for (int k = 0; k < 6; k++) 
							{
								if (prtcl->second_neighbour_list[j]->neighbour_list[k] == prtcl)
								{
									neigh = k;
									break;
								}
							}

							prtcl->second_neighbour_list[j]->second_neighbour_list[neigh] = NULL;      
							prtcl->second_neighbour_list[j]->second_neigh_break_Str[neigh] = 0.0;                
							prtcl->second_neighbour_list[j]->second_neighbour_is_broken = true;							
							prtcl->second_neighbour_list[j]->repulsion = true;                              
										
							prtcl->second_neigh_break_Str[j] = 0.0;
							prtcl->second_neighbour_list[j] = NULL;
							
						}
					}
					
					prtcl->second_neighbour_is_broken = true;
					prtcl->is_broken = true;
					prtcl->repulsion = true; 			
					
				}
			}
		}
	}
}

void ExperimentSettings::SetGaussianStrengthDistribution (Real g_mean, Real g_sigma, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax)
{

	Particle *prtcl;

    Real prob;			// probability from gauss
    Real k_break;			// pict spring
    Real ran_nb;			// rand number
    
    srand (time (0));

	// set strength for particles
    for (int i = 0; i < model->nb_of_particles; i++)
    {
		
		prtcl = &(model->particle_list[i]);
		
		if (prtcl->pos[0] < xmax && prtcl->pos[0] > xmin)
		{
			if (prtcl->pos[1] < ymax && prtcl->pos[1] > ymin)
			{
				if (prtcl->pos[2] < zmax && prtcl->pos[2] > zmin)
				{
					do
					{
						k_break = rand () / Real(RAND_MAX);

						k_break = (k_break - 0.5) * 8.0 * (8.0 * g_sigma);
						k_break = k_break + g_mean;

						prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
						prob = prob * (exp (-0.5 * (((k_break) - g_mean) / g_sigma) * (((k_break) - g_mean) / g_sigma)));
						prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

						ran_nb = rand () / Real(RAND_MAX);

						if (ran_nb <= prob)
						{
						    if (k_break <= 0.0)
						        ;
						    else
						    {
						        prtcl->br_strength *= k_break ;
						        break;
						    }
						}
					}
					while (1);
		    	}
		    }
        }
    }

    // set the breakingstrength for springs, based on particle strength
    for (int i = 0; i < model->nb_of_particles; i++)
    {
		prtcl = &(model->particle_list[i]);
		
        for (int j = 0; j < 12; j++)                            
        {
            if (prtcl->neighbour_list[j])
            {
				prtcl->break_Str[j] = (prtcl->br_strength + prtcl->neighbour_list[j]->br_strength) / 2.0;
            }
        }
        for (int j = 0; j < 6; j++)                            
        {
            if (prtcl->second_neighbour_list[j])
            {
				prtcl->second_neigh_break_Str[j] = (prtcl->br_strength + prtcl->second_neighbour_list[j]->br_strength) / 2.0;
			}
		}
    }
}

void ExperimentSettings::HardenParticles(Real factor, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax)
{
	for (int i = 0; i < model->nb_of_particles; i++)
	{
		if (model->particle_list[i].pos[0] > xmin && model->particle_list[i].pos[0] < xmax)
			if (model->particle_list[i].pos[1] > ymin && model->particle_list[i].pos[1] < ymax)
				if (model->particle_list[i].pos[2] > zmin && model->particle_list[i].pos[2] < zmax)
				{
					model->particle_list[i].young *= factor;
					model->particle_list[i].k1 *= factor;
					model->particle_list[i].k2 *= factor;
					model->particle_list[i].ks1 *= factor;
					model->particle_list[i].ks2 *= factor;
				}
	}
}

void ExperimentSettings::SetGaussianYoungandVDistribution ( Real g_mean, Real g_sigma, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax)
{
	
	Particle *prtcl;

    Real prob;			// probability from gauss
    Real k_spring;		// pict spring
    Real ran_nb;			// rand number

    srand (time (0));

    for (int i = 0; i < model->nb_of_particles; i++)
    {
		
		prtcl = &(model->particle_list[i]);
		
		if (prtcl->pos[0] < xmax && prtcl->pos[0] > xmin)
		{
			if (prtcl->pos[1] < ymax && prtcl->pos[1] > ymin)
			{
				if (prtcl->pos[2] < zmax && prtcl->pos[2] > zmin)
				{
					do
					{
						k_spring = rand () / Real(RAND_MAX);

						k_spring = (k_spring - 0.5) * 8.0 * (8.0 * g_sigma);
						k_spring = k_spring + g_mean;

						prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
						prob = prob * (exp (-0.5 * (((k_spring) - g_mean) / g_sigma) * (((k_spring) - g_mean) / g_sigma)));
						prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

						ran_nb = rand () / Real(RAND_MAX);

						if (ran_nb <= prob)
						{
						    if (k_spring <= 0.0)
						        ;
						    else
						    {
						        prtcl->SetYoungsModulusAndV(prtcl->real_young*k_spring, prtcl->v*k_spring);
						        break;
						    }
						}
					}
					while (1);
				}
			}
        }
    }
}

void ExperimentSettings::ChangeBreakingStrength(Real breaking_strength, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax)
{
	
	Particle *prtcl;

    for (int i=0; i<model->nb_of_particles; i++)
    {
		
		prtcl = &(model->particle_list[i]);
		
    	if (prtcl->pos[0] >= xmin && prtcl->pos[0] <= xmax)
    	{
			if (prtcl->pos[1] >= ymin && prtcl->pos[1] <= ymax)
			{
				if (prtcl->pos[2] >= zmin && prtcl->pos[2] <= zmax)
				{
					prtcl->br_strength = breaking_strength;
				}
			}
		}
    }
    
    // set the breakingstrength for springs, based on particle strength
    for (int i = 0; i < model->nb_of_particles; i++)
    {
		prtcl = &(model->particle_list[i]);
		
        for (int j = 0; j < 12; j++)                            
        {
            if (prtcl->neighbour_list[j])
            {
				prtcl->break_Str[j] = (prtcl->br_strength + prtcl->neighbour_list[j]->br_strength) / 2.0;
            }
        }
        for (int j = 0; j < 6; j++)                            
        {
            if (prtcl->second_neighbour_list[j])
            {
				prtcl->second_neigh_break_Str[j] = (prtcl->br_strength + prtcl->second_neighbour_list[j]->br_strength) / 2.0;
			}
		}
    }
}

void ExperimentSettings::ResetNoBreak()
{
    for (int i=0; i < model->nb_of_particles; i++)
    {

   		for (int j=0; j<12; j++)
          	model->particle_list[i].no_break[j] = false;

   		for (int j=0; j<6; j++)
         	model->particle_list[i].second_neigh_no_break[j] = false;
         
    }
}

// sets a no-break condition for all the particles below a certain threshold
// the no_break is set for 1st- and second neighbour particles
void ExperimentSettings::SetNoBreakY(Real depth)
{
    for (int i=0; i < model->nb_of_particles; i++)
    {
        if (model->particle_list[i].pos[1] < depth)
        {
            for (int j=0; j<12; j++)
            {
                if (model->particle_list[i].neighbour_list[j])
                {
                    model->particle_list[i].no_break[j] = true;

                    for (int k=0; k<12; k++)
                    {
                        if (model->particle_list[i].neighbour_list[j]->neighbour_list[k])
                        {
                            if (model->particle_list[i].neighbour_list[j]->neighbour_list[k] == &model->particle_list[i])
                            {
                                model->particle_list[i].neighbour_list[j]->no_break[k] = true;
                            }
                        }
                    }
                }
            }
            for (int j=0; j<6; j++)
            {
                if (model->particle_list[i].second_neighbour_list[j])
                {
                    model->particle_list[i].second_neigh_no_break[j] = true;

                    for (int k=0; k<6; k++)
                    {
                        if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k])
                        {
                            if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k] == &model->particle_list[i])
                            {
                                model->particle_list[i].second_neighbour_list[j]->second_neigh_no_break[k] = true;
                            }
                        }
                    }
                }
            }
        }
    }    
}

// sets a no-break condition for all the particles below a certain threshold
// the no_break is set for 1st- and second neighbour particles
void ExperimentSettings::SetNoBreakX(Real left, Real right)
{
    for (int i=0; i < model->nb_of_particles; i++)
    {
        if (model->particle_list[i].pos[0] < right && model->particle_list[i].pos[0] > left)
        {
            for (int j=0; j<12; j++)
            {
                if (model->particle_list[i].neighbour_list[j])
                {
                    model->particle_list[i].no_break[j] = true;

                    for (int k=0; k<12; k++)
                    {
                        if (model->particle_list[i].neighbour_list[j]->neighbour_list[k])
                        {
                            if (model->particle_list[i].neighbour_list[j]->neighbour_list[k] == &model->particle_list[i])
                            {
                                model->particle_list[i].neighbour_list[j]->no_break[k] = true;
                            }
                        }
                    }
                }
            }
            for (int j=0; j<6; j++)
            {
                if (model->particle_list[i].second_neighbour_list[j])
                {
                    model->particle_list[i].second_neigh_no_break[j] = true;

                    for (int k=0; k<6; k++)
                    {
                        if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k])
                        {
                            if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k] == &model->particle_list[i])
                            {
                                model->particle_list[i].second_neighbour_list[j]->second_neigh_no_break[k] = true;
                            }
                        }
                    }
                }
            }
        }
    }    
}

// Cuts the springs (1st and 2nd neighbour) which cross a predefined plane on a 
// certain x-position, normal to the x-axis. Also the upper/lower/left/right 
// borders of that plane are defined.
void ExperimentSettings::SeparateParticlesCrossingDistinctYZPlane(Real planex, Real plane_upper_y, 
                                                                  Real plane_lower_y, Real plane_front_z, Real plane_back_z)
{

    // at first check, if the given x-value for the plane is identical with x-values
    // of particles. If so move it a tiny bit to the left.
    for (int i=0; i < model->nb_of_particles; i++)
    {
        if (planex == model->particle_list[i].pos[0])
        {
            planex -= 0.000012345;
            break;
        }
    }

    // we are going to loop througl all particels. If a particle is left from the plane,
    // while its neighbour is right from the plane the spring will be deleted
    for (int i=0; i < model->nb_of_particles; i++)
    {

        if ( model->particle_list[i].pos[1] < plane_upper_y && model->particle_list[i].pos[1] >= plane_lower_y )
    	{
            if ( model->particle_list[i].pos[2] <= plane_back_z && model->particle_list[i].pos[2] >= plane_front_z )
            {
                if (fabs(planex - model->particle_list[i].pos[0]) <= model->second_neig_radius)
                {
                    for (int j=0; j < 6; j++)
                    {
                        if (model->particle_list[i].second_neighbour_list[j])
                        {
                            if ( model->particle_list[i].second_neighbour_list[j]->pos[0] > planex )
                            {
                                model->particle_list[i].neigh = j + 12;
                                model->particle_list[i].BreakBond();
                            }
                        }
                    }
                }

                if (fabs(planex - model->particle_list[i].pos[0]) <= model->particle_radius)
                {
                    for (int j=0; j < 12; j++)
                    {
                        if (model->particle_list[i].neighbour_list[j])
                        {
                            if (model->particle_list[i].neighbour_list[j]->pos[0] - planex > 0)
                            {
                                model->particle_list[i].neigh = j;
                                model->particle_list[i].BreakBond();
                            }
                        }
                    }
                }
            }
    	}
    }
}

// Severes springs crossing an arbitrarily positioned plane.
// It's defined by the upper- and lower x,y,z-boundary and affects the points therein
// Plane itself is defined by 3 points in X/Y/Z
//~ void ExperimentSettings::SeverSpringsCrossingPlane( Real p_1_x, Real p_1_y, Real p_1_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z, Real xlimit1, Real xlimit2, Real ylimit1, Real ylimit2, Real zlimit1, Real zlimit2 )
//~ {
		//~ 
	//~ Particle *prtcl = NULL;
	//~ 
	//~ Real xupper = xlimit1, xlower = xlimit2, yupper = ylimit1, ylower = ylimit2, zupper = zlimit1, zlower = zlimit2;
	//~ if (zlimit1 < zlimit2)
	//~ {
		//~ zupper = zlimit2;
		//~ zlower = zlimit1;
	//~ }
	//~ if (xlimit1 < xlimit2)
	//~ {
		//~ xupper = xlimit2;
		//~ xlower = xlimit1;
	//~ }
	//~ if (ylimit1 < ylimit2)
	//~ {
		//~ yupper = ylimit2;
		//~ ylower = ylimit1;
	//~ }
	//~ 
	//~ Real a1 = p_2_x - p_1_x;
	//~ Real a2 = p_2_y - p_1_y;
	//~ Real a3 = p_2_z - p_1_z;
	//~ 
	//~ Real b1 = p_3_x - p_1_x;
	//~ Real b2 = p_3_y - p_1_y;
	//~ Real b3 = p_3_z - p_1_z;
	//~ 
	//~ Real normal_vec_x = a2*b3 - a3*b2;
	//~ Real normal_vec_y = b1*a3 - a1*b3;
	//~ Real normal_vec_z = a1*b2 - a2*b1;
	//~ 
	//~ //normalize:
	//~ Real norm = sqrt(normal_vec_x*normal_vec_x + normal_vec_y*normal_vec_y + normal_vec_z*normal_vec_z);
	//~ normal_vec_x /= norm;
	//~ normal_vec_y /= norm;
	//~ normal_vec_z /= norm;
	//~ 
	//~ 
	//~ 
//~ }

void ExperimentSettings::SeverSpringsCrossingPlane( Real p_1_x, Real p_1_y, Real p_1_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z, Real xlimit1, Real xlimit2, Real ylimit1, Real ylimit2, Real zlimit1, Real zlimit2 )
{
	
	Real xupper = xlimit1, xlower = xlimit2, yupper = ylimit1, ylower = ylimit2, zupper = zlimit1, zlower = zlimit2;
	if (zlimit1 < zlimit2)
	{
		zupper = zlimit2;
		zlower = zlimit1;
	}
	if (xlimit1 < xlimit2)
	{
		xupper = xlimit2;
		xlower = xlimit1;
	}
	if (ylimit1 < ylimit2)
	{
		yupper = ylimit2;
		ylower = ylimit1;
	}
	
	Particle *prtcl = NULL;
	
	Real a1 = p_2_x - p_1_x;
	Real a2 = p_2_y - p_1_y;
	Real a3 = p_2_z - p_1_z;
	
	Real b1 = p_3_x - p_2_x;
	Real b2 = p_3_y - p_2_y;
	Real b3 = p_3_z - p_2_z;
	
	Real normal_vec_x = a2*b3 - a3*b2;
	Real normal_vec_y = b1*a3 - a1*b3;
	Real normal_vec_z = a1*b2 - a2*b1;
	
	//normalize:
	Real norm = sqrt(normal_vec_x*normal_vec_x + normal_vec_y*normal_vec_y + normal_vec_z*normal_vec_z);
	normal_vec_x /= norm;
	normal_vec_y /= norm;
	normal_vec_z /= norm;
		
	Real d = -(p_1_x*normal_vec_x + p_1_y*normal_vec_y + p_1_z*normal_vec_z);
	Real p, pn;
	
	bool cond;
	
	// we filter the particles in question (>ylower, <ylower, etc.)
    for (int i=0; i < model->nb_of_particles; i++)
    {
        if ( model->particle_list[i].pos[0] <= xupper && model->particle_list[i].pos[0] >= xlower )
    	{
	        if ( model->particle_list[i].pos[1] <= yupper && model->particle_list[i].pos[1] >= ylower )
	    	{
	            if ( model->particle_list[i].pos[2] <= zupper && model->particle_list[i].pos[2] >= zlower )
	            {
					
					prtcl = &model->particle_list[i];
					
					/********
					check if the particle and its neighbours are on different sides of the plane
					********/
					p = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
					p = p + normal_vec_z*prtcl->pos[2] + d;
					
					// loop second neighbours
					for (int j=0; j < 6; j++)
					{
						if (model->particle_list[i].second_neighbour_list[j])
						{						
							prtcl = model->particle_list[i].second_neighbour_list[j];
							
							pn = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
							pn = pn + normal_vec_z*prtcl->pos[2] + d;
					
							cond = false;
							if (p > 0.0 && pn < 0.0)
								cond = true;
							if (p < 0.0 && pn > 0.0)
								cond = true;
								
							// check if crossing (periodic) boundaries
							//~ if (prtcl->is_left_boundary && model->particle_list[i].is_right_boundary)
								//~ cond = false;
							//~ if (prtcl->is_right_boundary && model->particle_list[i].is_left_boundary)
								//~ cond = false;
							//~ if (prtcl->is_front_boundary && model->particle_list[i].is_back_boundary)
								//~ cond = false;
							//~ if (prtcl->is_back_boundary && model->particle_list[i].is_front_boundary)
								//~ cond = false;
										
							if ( cond )
							{
								model->particle_list[i].neigh = j + 12;
								model->particle_list[i].BreakBond();
							}
						}
					}
			
					for (int j=0; j < 12; j++)
					{
						if (model->particle_list[i].neighbour_list[j])
						{					
							prtcl = model->particle_list[i].neighbour_list[j];
							
							pn = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
							pn = pn + normal_vec_z*prtcl->pos[2] + d;
					
							cond = false;
							if (p > 0.0 && pn < 0.0)
								cond = true;
							if (p < 0.0 && pn > 0.0)
								cond = true;
								
							// check if crossing (periodic) boundaries
							//~ if (prtcl->is_left_boundary && model->particle_list[i].is_right_boundary)
								//~ cond = false;
							//~ if (prtcl->is_right_boundary && model->particle_list[i].is_left_boundary)
								//~ cond = false;
							//~ if (prtcl->is_front_boundary && model->particle_list[i].is_back_boundary)
								//~ cond = false;
							//~ if (prtcl->is_back_boundary && model->particle_list[i].is_front_boundary)
								//~ cond = false;
										
							if ( cond )
							{
								model->particle_list[i].neigh = j;
								model->particle_list[i].BreakBond();
							}
						}
					}
	            }
	    	}
		}
    }
}

void ExperimentSettings::SeverSpringsCrossingCircularPlane( Real radius, Real center_x, Real center_y, Real center_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z )
{
	
	Particle *prtcl = NULL;
	
	Real a1 = p_2_x - center_x;
	Real a2 = p_2_y - center_y;
	Real a3 = p_2_z - center_z;
	
	Real b1 = p_3_x - p_2_x;
	Real b2 = p_3_y - p_2_y;
	Real b3 = p_3_z - p_2_z;
	
	Real normal_vec_x = a2*b3 - a3*b2;
	Real normal_vec_y = b1*a3 - a1*b3;
	Real normal_vec_z = a1*b2 - a2*b1;
	
	//normalize:
	Real norm = sqrt(normal_vec_x*normal_vec_x + normal_vec_y*normal_vec_y + normal_vec_z*normal_vec_z);
	normal_vec_x /= norm;
	normal_vec_y /= norm;
	normal_vec_z /= norm;
		
	Real d = -(center_x*normal_vec_x + center_y*normal_vec_y + center_z*normal_vec_z);
	Real p, pn;
	
	bool cond;
	
	Real dx, dy, dz, dist;
	
	// we filter the particles in question (>ylower, <ylower, etc.)
    for (int i=0; i < model->nb_of_particles; i++)
    {
		dx = center_x - model->particle_list[i].pos[0];
		dy = center_y - model->particle_list[i].pos[1];
		dz = center_z - model->particle_list[i].pos[2];
		
		dist = sqrt(dx*dx + dy*dy + dz*dz);
		
        if ( dist < radius )
    	{

			prtcl = &model->particle_list[i];
			
			/********
			check if the particle and its neighbours are on different sides of the plane
			********/
			p = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
			p = p + normal_vec_z*prtcl->pos[2] + d;
			
			// loop second neighbours
			for (int j=0; j < 6; j++)
			{
				if (model->particle_list[i].second_neighbour_list[j])
				{						
					prtcl = model->particle_list[i].second_neighbour_list[j];
					
					pn = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
					pn = pn + normal_vec_z*prtcl->pos[2] + d;
			
					cond = false;
					if (p > 0.0 && pn < 0.0)
						cond = true;
					if (p < 0.0 && pn > 0.0)
						cond = true;
						
					// check if crossing (periodic) boundaries
					if (prtcl->is_left_boundary && model->particle_list[i].is_right_boundary)
						cond = false;
					if (prtcl->is_right_boundary && model->particle_list[i].is_left_boundary)
						cond = false;
					if (prtcl->is_front_boundary && model->particle_list[i].is_back_boundary)
						cond = false;
					if (prtcl->is_back_boundary && model->particle_list[i].is_front_boundary)
						cond = false;
						
					dx = center_x - prtcl->pos[0];
					dy = center_y - prtcl->pos[1];
					dz = center_z - prtcl->pos[2];
					
					dist = sqrt(dx*dx + dy*dy + dz*dz);
					
					if (dist > radius)
						cond = false;
								
					if ( cond )
					{
						model->particle_list[i].neigh = j + 12;
						model->particle_list[i].BreakBond();
					}
				}
			}
	
			for (int j=0; j < 12; j++)
			{
				if (model->particle_list[i].neighbour_list[j])
				{					
					prtcl = model->particle_list[i].neighbour_list[j];
					
					pn = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
					pn = pn + normal_vec_z*prtcl->pos[2] + d;
			
					cond = false;
					if (p > 0.0 && pn < 0.0)
						cond = true;
					if (p < 0.0 && pn > 0.0)
						cond = true;
						
					// check if crossing (periodic) boundaries
					if (prtcl->is_left_boundary && model->particle_list[i].is_right_boundary)
						cond = false;
					if (prtcl->is_right_boundary && model->particle_list[i].is_left_boundary)
						cond = false;
					if (prtcl->is_front_boundary && model->particle_list[i].is_back_boundary)
						cond = false;
					if (prtcl->is_back_boundary && model->particle_list[i].is_front_boundary)
						cond = false;
								
					dx = center_x - prtcl->pos[0];
					dy = center_y - prtcl->pos[1];
					dz = center_z - prtcl->pos[2];
					
					dist = sqrt(dx*dx + dy*dy + dz*dz);
					
					if (dist > radius)
						cond = false;
								
					if ( cond )
					{
						model->particle_list[i].neigh = j;
						model->particle_list[i].BreakBond();
					}
				}
			}
		}
    }
}

// Severes springs crossing an arbitrarily positioned triangular area
void ExperimentSettings::SeverSpringsCrossingTriangle( Real p_1_x, Real p_1_y, Real p_1_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z )
{
	
	/*
	 * This function is very much similar to the SeverSpringsCrossingPlane-fct
	 * above.
	 * It adds that every point is projected onto the triangle (after rotating 
	 * it on the yz-plane, so that x only has to be set 0 for a projection),
	 * then the angle from this (projected) point with all the triangle-edges
	 * are calculated and added up. If the point is inside the triangle, then 
	 * sum of angles will be 180.
	 * Then check, if this point has neighbours on the other side of the x-beam.
	 */
	
	Particle *prtcl = NULL;
	
	Real a1 = p_2_x - p_1_x;
	Real a2 = p_2_y - p_1_y;
	Real a3 = p_2_z - p_1_z;
	
	Real b1 = p_3_x - p_2_x;
	Real b2 = p_3_y - p_2_y;
	Real b3 = p_3_z - p_2_z;
	
	Real normal_vec_x = a2*b3 - a3*b2;
	Real normal_vec_y = b1*a3 - a1*b3;
	Real normal_vec_z = a1*b2 - a2*b1;
	
	//normalize:
	Real norm = sqrt(normal_vec_x*normal_vec_x + normal_vec_y*normal_vec_y + normal_vec_z*normal_vec_z);
	normal_vec_x /= norm;
	normal_vec_y /= norm;
	normal_vec_z /= norm;
		
	Real d = -(p_1_x*normal_vec_x + p_1_y*normal_vec_y + p_1_z*normal_vec_z);
	
	Real p, pn, norm1, norm2, norm3, sum;
	Real pa1, pa2, pa3, pb1, pb2, pb3, pc1, pc2, pc3, p1, p2, p3;
	Real r1, r2, r3, t, i1, i2, i3;
	bool cond;
	
	// we filter the particles in question (check if they are on a normal to the triangle)
    for (int i=0; i < model->nb_of_particles; i++)
    {
		prtcl = &model->particle_list[i];
		
		/********
		check if the particle and its neighbours are on different sides of the plane
		********/
		p = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
		p = p + normal_vec_z*prtcl->pos[2] + d;
		
		p1 = prtcl->pos[0];
		p2 = prtcl->pos[1];
		p3 = prtcl->pos[2];
		
		// loop second neighbours
		for (int j=0; j < 6; j++)
		{
			if (model->particle_list[i].second_neighbour_list[j])
			{						
				prtcl = model->particle_list[i].second_neighbour_list[j];
				
				pn = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
				pn = pn + normal_vec_z*prtcl->pos[2] + d;
		
				cond = false;
				if (p > 0.0 && pn < 0.0)
					cond = true;
				if (p < 0.0 && pn > 0.0)
					cond = true;
					
				// check if crossing (periodic) boundaries
				if (prtcl->is_left_boundary && model->particle_list[i].is_right_boundary)
					cond = false;
				if (prtcl->is_right_boundary && model->particle_list[i].is_left_boundary)
					cond = false;
				if (prtcl->is_front_boundary && model->particle_list[i].is_back_boundary)
					cond = false;
				if (prtcl->is_back_boundary && model->particle_list[i].is_front_boundary)
					cond = false;
							
				if ( cond )
				{
					/********
					check if bond crosses triangle:
					*********/
    
					r1 = prtcl->pos[0] - p1;
					r2 = prtcl->pos[1] - p2;
					r3 = prtcl->pos[2] - p3;
					
					t = -(d + (normal_vec_x*p1 + normal_vec_y*p2 + normal_vec_z*p3));
					t = t / (normal_vec_x*r1 + normal_vec_y*r2 + normal_vec_z*r3);
					
					i1 = p1 + r1*t;
					i2 = p2 + r2*t;
					i3 = p3 + r3*t;
					
					pa1 = p_1_x - i1;
					pa2 = p_1_y - i2;
					pa3 = p_1_z - i3;
					
					pb1 = p_2_x - i1;
					pb2 = p_2_y - i2;
					pb3 = p_2_z - i3;
					
					pc1 = p_3_x - i1;
					pc2 = p_3_y - i2;
					pc3 = p_3_z - i3;
					
					norm1 = sqrt(pa1*pa1 + pa2*pa2 + pa3*pa3);
					norm2 = sqrt(pb1*pb1 + pb2*pb2 + pb3*pb3);
					norm3 = sqrt(pc1*pc1 + pc2*pc2 + pc3*pc3);
					
					sum = acos((pa1*pb1 + pa2*pb2 + pa3*pb3) / (norm1*norm2));
					sum += acos((pa1*pc1 + pa2*pc2 + pa3*pc3) / (norm1*norm3));
					sum += acos((pc1*pb1 + pc2*pb2 + pc3*pb3) / (norm3*norm2));
				
					if (sum > 2.0*pi-0.01 && sum < 2.0*pi+0.01)
					{
						model->particle_list[i].neigh = j + 12;
						model->particle_list[i].BreakBond();
					}
				}
			}
		}

		for (int j=0; j < 12; j++)
		{
			if (model->particle_list[i].neighbour_list[j])
			{					
				prtcl = model->particle_list[i].neighbour_list[j];
				
				pn = normal_vec_x*prtcl->pos[0] + normal_vec_y*prtcl->pos[1];
				pn = pn + normal_vec_z*prtcl->pos[2] + d;
		
				cond = false;
				if (p > 0.0 && pn < 0.0)
					cond = true;
				if (p < 0.0 && pn > 0.0)
					cond = true;
					
				// check if crossing (periodic) boundaries
				if (prtcl->is_left_boundary && model->particle_list[i].is_right_boundary)
					cond = false;
				if (prtcl->is_right_boundary && model->particle_list[i].is_left_boundary)
					cond = false;
				if (prtcl->is_front_boundary && model->particle_list[i].is_back_boundary)
					cond = false;
				if (prtcl->is_back_boundary && model->particle_list[i].is_front_boundary)
					cond = false;
							
				if ( cond )
				{
					/********
					check if bond crosses triangle:
					*********/
    
					r1 = prtcl->pos[0] - p1;
					r2 = prtcl->pos[1] - p2;
					r3 = prtcl->pos[2] - p3;
					
					t = -(d + (normal_vec_x*p1 + normal_vec_y*p2 + normal_vec_z*p3));
					t = t / (normal_vec_x*r1 + normal_vec_y*r2 + normal_vec_z*r3);
					
					i1 = p1 + r1*t;
					i2 = p2 + r2*t;
					i3 = p3 + r3*t;
					
					pa1 = p_1_x - i1;
					pa2 = p_1_y - i2;
					pa3 = p_1_z - i3;
					
					pb1 = p_2_x - i1;
					pb2 = p_2_y - i2;
					pb3 = p_2_z - i3;
					
					pc1 = p_3_x - i1;
					pc2 = p_3_y - i2;
					pc3 = p_3_z - i3;
					
					norm1 = sqrt(pa1*pa1 + pa2*pa2 + pa3*pa3);
					norm2 = sqrt(pb1*pb1 + pb2*pb2 + pb3*pb3);
					norm3 = sqrt(pc1*pc1 + pc2*pc2 + pc3*pc3);
					
					sum = acos((pa1*pb1 + pa2*pb2 + pa3*pb3) / (norm1*norm2));
					sum += acos((pa1*pc1 + pa2*pc2 + pa3*pc3) / (norm1*norm3));
					sum += acos((pc1*pb1 + pc2*pb2 + pc3*pb3) / (norm3*norm2));
				
					if (sum > 2.0*pi-0.01 && sum < 2.0*pi+0.01)
					{
						model->particle_list[i].neigh = j;
						model->particle_list[i].BreakBond();
					}
				}
			}
		}
	}
}

void ExperimentSettings::WeakenSpringsCrossingDistinctYZPlane(Real young_factor, Real break_factor, Real planex, Real plane_upper_y,
                                                              Real plane_lower_y, Real plane_front_z, Real plane_back_z)
{

	bool done = false;
	
    // at first check, if the given x-value for the plane is identical with x-values
    // of particles. If so move it a tiny bit to the left.
    for (int i=0; i < model->nb_of_particles; i++)
    {
        if (planex == model->particle_list[i].pos[0])
        {
            planex -= 0.000012345;
            break;
        }
    }

    // we are going to loop througl all particels. If a particle is left from the plane,
    // while its neighbour is right from the plane the spring will be deleted
    for (int i=0; i < model->nb_of_particles; i++)
    {
		done = false;
		
        if ( model->particle_list[i].pos[1] < plane_upper_y && model->particle_list[i].pos[1] >= plane_lower_y )
        {
            if ( model->particle_list[i].pos[2] >= plane_back_z && model->particle_list[i].pos[2] <= plane_front_z )
            {
                if (fabs(planex - model->particle_list[i].pos[0]) <= model->second_neig_radius)
                {
                    for (int j=0; j < 6; j++)
                    {
                        if (model->particle_list[i].second_neighbour_list[j])
                        {
                            if ( model->particle_list[i].second_neighbour_list[j]->pos[0] > planex )
                            {
                                model->particle_list[i].neigh = j + 12;
                                model->particle_list[i].WeakenBond(young_factor, break_factor);
                                
                                if (!done)
                                {
									model->particle_list[i].young *= young_factor;
									model->particle_list[i].k1 *= young_factor;
									model->particle_list[i].k2 *= young_factor;
									model->particle_list[i].ks1 *= young_factor;
									model->particle_list[i].ks2 *= young_factor;
									done = true;
								}
                            }
                        }
                    }
                }

                if (fabs(planex - model->particle_list[i].pos[0]) <= model->particle_radius)
                {
                    for (int j=0; j < 12; j++)
                    {
                        if (model->particle_list[i].neighbour_list[j])
                        {
                            if (model->particle_list[i].neighbour_list[j]->pos[0] - planex > 0)
                            {
                                model->particle_list[i].neigh = j;
                                model->particle_list[i].WeakenBond(young_factor, break_factor);
                            
                               	if (!done)
                                {
									model->particle_list[i].young *= young_factor;
									model->particle_list[i].k1 *= young_factor;
									model->particle_list[i].k2 *= young_factor;
									model->particle_list[i].ks1 *= young_factor;
									model->particle_list[i].ks2 *= young_factor;
									done = true;
								}
                            }
                        }
                    }
                }
            }
        }
    }
}

// enables the breaking of bonds along the walls in z
void ExperimentSettings::EnableSideWallBreakingInZ()
{
	
	// enable breaking anywhere
    for (int i=0; i<model->nb_of_particles; i++)
    {
        if (!model->particle_list[i].is_bottom_boundary)
        {
			for (int j=0; j<12; j++)
				if (model->particle_list[i].neighbour_list[j])
					if (!model->particle_list[i].neighbour_list[j]->is_bottom_boundary)
						model->particle_list[i].no_break[j] = false;
			for (int j=0; j<6; j++)
				if (model->particle_list[i].second_neighbour_list[j])
					if (!model->particle_list[i].second_neighbour_list[j]->is_bottom_boundary)
						model->particle_list[i].second_neigh_no_break[j] = false;
        }
    }
    
    // re-disable breaking at the left and the right boundary
    Particle *prtcl;
    for (int i=0; i<model->nb_of_particles; i++)
    {
        if (model->particle_list[i].is_left_boundary || model->particle_list[i].is_right_boundary)
        {
			for (int j=0; j<12; j++)
				if (model->particle_list[i].neighbour_list[j])
				{
					model->particle_list[i].no_break[j] = true;

					prtcl = model->particle_list[i].neighbour_list[j];
					
					for (int k=0; k<12; k++)
					{
						if (prtcl->neighbour_list[k])
						{
							if (prtcl->neighbour_list[k] == &(model->particle_list[i]))
							{
								prtcl->no_break[k] = true;
							}
						}
					}
				}
					
			for (int j=0; j<6; j++)
			{
				if (model->particle_list[i].second_neighbour_list[j])
				{
					model->particle_list[i].second_neigh_no_break[j] = true;
				
					prtcl = model->particle_list[i].second_neighbour_list[j];
					
					for (int k=0; k<6; k++)
					{
						if (prtcl->second_neighbour_list[k])
						{
							if (prtcl->second_neighbour_list[k] == &(model->particle_list[i]))
							{
								prtcl->second_neigh_no_break[k] = true;
							}
						}
					}
				}
			}
        }
    }
}

void ExperimentSettings::EnableSideWallBreakingInZAndX()
{
    for (int i=0; i<model->nb_of_particles; i++)
    {
        if (!model->particle_list[i].is_bottom_boundary)
        {
			for (int j=0; j<12; j++)
				if (model->particle_list[i].neighbour_list[j])
					if (!model->particle_list[i].neighbour_list[j]->is_bottom_boundary)
						model->particle_list[i].no_break[j] = false;
			for (int j=0; j<6; j++)
				if (model->particle_list[i].second_neighbour_list[j])
					if (!model->particle_list[i].second_neighbour_list[j]->is_bottom_boundary)
						model->particle_list[i].second_neigh_no_break[j] = false;
        }
    }
}

void ExperimentSettings::Unfix_z()
{
    for (int i = 0; i<model->nb_of_particles; i++)
    {
    	model->particle_list[i].fix_z = false;
    }
}

void ExperimentSettings::Unfix_y()
{
    for (int i = 0; i<model->nb_of_particles; i++)
    {
		model->particle_list[i].fix_y = false;
    }
}

void ExperimentSettings::Unfix_x()
{
    for (int i = 0; i<model->nb_of_particles; i++)
    {
		model->particle_list[i].fix_x = false;
    }
}

void ExperimentSettings::Pin_Bottom_Boundary()
{
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].is_bottom_boundary)
		{
			model->particle_list[i].fix_x = true;
			model->particle_list[i].fix_y = true;
			model->particle_list[i].fix_z = true;
		}
	}
	
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].is_bottom_boundary)
		{
			for (int j=0; j<12; j++)
			{
				if (model->particle_list[i].neighbour_list[j])
				{
					model->particle_list[i].no_break[j] = true;

					for (int k=0; k<12; k++)
					{
						if (model->particle_list[i].neighbour_list[j]->neighbour_list[k])
						{
							if (model->particle_list[i].neighbour_list[j]->neighbour_list[k] == &model->particle_list[i])
							{
								model->particle_list[i].neighbour_list[j]->no_break[k] = true;
							}
						}
					}
				}
			}
			for (int j=0; j<6; j++)
			{
				if (model->particle_list[i].second_neighbour_list[j])
				{
					model->particle_list[i].second_neigh_no_break[j] = true;

					for (int k=0; k<6; k++)
					{
						if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k])
						{
							if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k] == &model->particle_list[i])
							{
								model->particle_list[i].second_neighbour_list[j]->second_neigh_no_break[k] = true;
							}
						}
					}
				}
			}
		}
	}
}

void ExperimentSettings::Pin_Bottom_Boundary_Several_Layer(int nb)
{
	Real y = nb * 2.0 * sqrt(2.0) / sqrt(3.0) * model->particle_radius + model->particle_radius + 0.01 * model->particle_radius;
	
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].pos[1] <= y)
		{
			
			model->particle_list[i].is_boundary = true;
			model->particle_list[i].fix_x = true;
			model->particle_list[i].fix_y = true;
			model->particle_list[i].fix_z = true;
			
			for (int j=0; j<12; j++)
			{
				if (model->particle_list[i].neighbour_list[j])
				{
					model->particle_list[i].no_break[j] = true;

					for (int k=0; k<12; k++)
					{
						if (model->particle_list[i].neighbour_list[j]->neighbour_list[k])
						{
							if (model->particle_list[i].neighbour_list[j]->neighbour_list[k] == &model->particle_list[i])
							{
								model->particle_list[i].neighbour_list[j]->no_break[k] = true;
							}
						}
					}
				}
			}
			for (int j=0; j<6; j++)
			{
				if (model->particle_list[i].second_neighbour_list[j])
				{
					model->particle_list[i].second_neigh_no_break[j] = true;

					for (int k=0; k<6; k++)
					{
						if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k])
						{
							if (model->particle_list[i].second_neighbour_list[j]->second_neighbour_list[k] == &model->particle_list[i])
							{
								model->particle_list[i].second_neighbour_list[j]->second_neigh_no_break[k] = true;
							}
						}
					}
				}
			}
		}
	}
}

void ExperimentSettings::Pin_Top_Boundary()
{
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].is_top_boundary)
		{
			model->particle_list[i].fix_x = true;
			model->particle_list[i].fix_y = true;
			model->particle_list[i].fix_z = true;
		}
	}
}

void ExperimentSettings::Pin_Left_Boundary()
{
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].is_left_boundary)
		{
			model->particle_list[i].fix_x = true;
			model->particle_list[i].fix_y = true;
			model->particle_list[i].fix_z = true;
		}
	}
}

void ExperimentSettings::Pin_Right_Boundary()
{
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].is_right_boundary)
		{
			model->particle_list[i].fix_x = true;
			model->particle_list[i].fix_y = true;
			model->particle_list[i].fix_z = true;
		}
	}
}

void ExperimentSettings::Pin_Front_Boundary()
{
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].is_front_boundary)
		{
			model->particle_list[i].fix_x = true;
			model->particle_list[i].fix_y = true;
			model->particle_list[i].fix_z = true;
		}
	}
}

void ExperimentSettings::Pin_Back_Boundary()
{
	for (int i=0; i<model->nb_of_particles; i++)
	{
		if (model->particle_list[i].is_back_boundary)
		{
			model->particle_list[i].fix_x = true;
			model->particle_list[i].fix_y = true;
			model->particle_list[i].fix_z = true;
		}
	}
}

void ExperimentSettings::Set_Tensor_Failure_Mode(bool stress_tensor_based_failure)
{
	model->stress_tensor_based_failure = stress_tensor_based_failure;

	for (int i=0; i<model->nb_of_particles; i++)
	{
		model->particle_list[i].stress_tensor_based_failure = stress_tensor_based_failure;
	}
}

void ExperimentSettings::Set_Angle_Of_Internal_Friction(Real angle_of_internal_friction, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax)
{	
    for (int i=0; i<model->nb_of_particles; i++)
    {
		if (model->particle_list[i].pos[0] > xmin && model->particle_list[i].pos[0] < xmax)
			if (model->particle_list[i].pos[1] > ymin && model->particle_list[i].pos[1] < ymax)
				if (model->particle_list[i].pos[2] > zmin && model->particle_list[i].pos[2] < zmax)
			        model->particle_list[i].angle_of_internal_friction = angle_of_internal_friction;
	}
}

void ExperimentSettings::Activate_Mohr_Coulomb_Failure(bool activate)
{
	model->Activate_Mohr_Coulomb_Failure(activate);
}

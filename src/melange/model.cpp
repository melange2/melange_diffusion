/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "model.h"

Model::Model()
{
	// 'dummy'-constructor
	// real initialization is done by user,
	// either with 'CreateNewModel()'
	// or with 'OpenModelFromFile()'
}

Model::~Model()
{
}

void Model::CreateNewModel(int resolution_in_x, Real xlength, Real ylength, Real zlength, 
	bool is_second_neighbour_model, bool repulsion, 
	bool walls_in_x, bool walls_in_z, 
	bool periodic_boundaries_in_x, bool periodic_boundaries_in_z)
{
	
	Model::repulsion = repulsion;
	Model::walls_in_x = walls_in_x;
	Model::walls_in_z = walls_in_z;
    Model::is_second_neighbour_model = is_second_neighbour_model;
	
    if (!repulsion && !is_second_neighbour_model)
    {
		cout << "	No particle interactions are defined!\n";
		cout << "	Enable at least one of the first-neighbour-/second-neighbour- or the repulsion-flag!\n";
		exit(0);
	}
	
    // start-setting, as in the editor window
    PrepareNewLattice(xlength, ylength, zlength, resolution_in_x, periodic_boundaries_in_x, periodic_boundaries_in_z);
    
    SetHullIntersections();

    for (int i=0; i<nb_of_particles; i++)
    {
		particle_list[i].InitModelFlags( repulsion, is_second_neighbour_model, walls_in_x, walls_in_z );
	}
	
	SetBackPtr();
    
    // model dimensions in meter (100.000 == 100km)
    scale_factor = 100000;	
	use_gravity_force = false;
	timestep = 0.0;
	
	viscoelastic = false;
	
	// standard is average of the upper crust
	standard_real_young = 2700.0;
	standard_v = 0.25;

	// whether spring failure is calculated from the full stress tensor or from the spring extension
	stress_tensor_based_failure = false;

	// Mohr-Coulomb (shear) failure activated? Default = yes
	mohr_coulomb_failure = true;
	
}

void Model::SetHullIntersections()
{
	Particle *prtcl;

	for (int i=0; i<nb_of_particles; i++)
	{
		prtcl = &(particle_list[i]);

		for (int j=0; j<12; j++)
		{
			if (prtcl->neighbour_list[j])
			{
				prtcl->neig_hull_intersect[j][0] = prtcl->neigpos[j][0]/2.0;
				prtcl->neig_hull_intersect[j][1] = prtcl->neigpos[j][1]/2.0;
				prtcl->neig_hull_intersect[j][2] = prtcl->neigpos[j][2]/2.0;
			}
		}
		for (int j=0; j<6; j++)
		{
			if (prtcl->second_neighbour_list[j])
			{
				prtcl->sec_neig_hull_intersect[j][0] = prtcl->secneigpos[j][0]/2.0;
				prtcl->sec_neig_hull_intersect[j][1] = prtcl->secneigpos[j][1]/2.0;
				prtcl->sec_neig_hull_intersect[j][2] = prtcl->secneigpos[j][2]/2.0;
			}
		}
	}	
}

void Model::SetBackPtr()
{
	
	int nb = -1;
	Particle *prtcl, *neig;
	
	for (int i=0; i<nb_of_particles; i++)
	{
		prtcl = &particle_list[i];
				
		if (is_second_neighbour_model)
		{
			// next neighbours first
			for (int j=0; j<12; j++)
			{
				if (prtcl->neighbour_list[j])
				{
					neig = prtcl->neighbour_list[j];
					
					for (int k=0; k<12; k++)
					{
						if (neig->neighbour_list[k])
						{
							if (prtcl == neig->neighbour_list[k])
							{
								nb = k;
								break;
							}
						}
					}
					prtcl->neig_back_ptr[nb] = j;
				}
			}
			
			// then 2nd next neighbours
			for (int j=0; j<6; j++)
			{
				if (prtcl->second_neighbour_list[j])
				{
					neig = prtcl->second_neighbour_list[j];
					
					for (int k=0; k<6; k++)
					{
						if (neig->second_neighbour_list[k])
						{
							if (prtcl == neig->second_neighbour_list[k])
							{
								nb = k;
								break;
							}
						}
					}
					prtcl->sec_neig_back_ptr[nb] = j;
				}
			}
		}
		
	}
}

// name: Model::PrepareNewLattice
// @param
// @return
void Model::PrepareNewLattice(Real x_size, Real y_size, Real z_size, int x_particles, bool use_periodic_boundaries_in_x, bool use_periodic_boundaries_in_z)
{
    CalcSizeAndNbOfParticlesAndSystemDimensions ( x_size, y_size, z_size, x_particles );

    xlength_initial = xlength;
    ylength_initial = ylength;
    zlength_initial = zlength;

    Model::use_periodic_boundaries_in_x = use_periodic_boundaries_in_x;
	Model::use_periodic_boundaries_in_z = use_periodic_boundaries_in_z;

    particle_list = new Particle[nb_of_particles];
    	
    for (int i=0; i<nb_of_particles; i++)
    {
        particle_list[i].nb = i;
    }

    for (int i=0; i<nb_of_particles;i++)
    {
        particle_list[i].use_periodic_boundaries_in_x = use_periodic_boundaries_in_x;
        particle_list[i].use_periodic_boundaries_in_z = use_periodic_boundaries_in_z;
    }

    for (int i=0; i<nb_of_particles;i++)
    {
        particle_list[i].SetGeometry(particle_radius, second_neig_radius);
    }

    for (int i=0; i<nb_of_particles;i++)
    {
        particle_list[i].SetYoungsModulusAndV(standard_real_young, standard_v);
    }

	for (int i=0; i<nb_of_particles; i++)
	{
		particle_list[i].wrapping_constant_x = wrapping_constant_x;
		particle_list[i].wrapping_constant_z = wrapping_constant_z;
	}
	
	for (int i=0; i<nb_of_particles; i++)
	{
		particle_list[i].xlength = xlength;
		particle_list[i].ylength = ylength;
		particle_list[i].zlength = zlength;
	}

    MakeHexagonalLattice(xlength, ylength, zlength);

	// indicates the initial position of the particls. used to visualize movements.
    for (int i=0; i<nb_of_particles; i++)
    {
       	particle_list[i].xlayer = particle_list[i].pos[0] / xlength; 
       	particle_list[i].ylayer = particle_list[i].pos[1] / ylength;
       	particle_list[i].zlayer = particle_list[i].pos[2] / zlength;
    }

    // define the neighbours of each particle (12 + 6)
    // Very important: requires a properly defined repulsion-box, where the 
    // spacing is set to 2*particle_radius
    MakeRepBox(true);
    
    DefineParticleNeighbours();
	
	/*
    * 	now that the neighbours are known, the wall-/egde- and corner-particles can be set
	*/  
	// this marks boudnary-particels in general    
    MarkNextAndSecondNextBoundaryParticles();
    // this marks the top-boundaries etc, only FIRST neighbours
	MarkTopBottomLeftRightFrontBackNextNeighbourBoundaryParticles();
    // this marks the top-boundaries etc, only SECOND neighbours
	if(is_second_neighbour_model)
	    MarkTopBottomLeftRightFrontBackSecondNextNeighbourBoundaryParticles();
	
	// now, finally, we can fix the boundary particles, so they cannot move into certain directions
	FixBoundaryParticles();

	// now we call MakeRepBox again, in the non-definition-mode
	MakeRepBox();

    // if we use periodic boundary conditions, we have to connect the
    // particles along the x-walls.
    // also, we have to re-allow these particles to move in x.
    if (use_periodic_boundaries_in_x)
        MakePeriodicBoundariesOnXAxis();
    if (use_periodic_boundaries_in_z)
        MakePeriodicBoundariesOnZAxis();
        
	AssignParticleNeighbourPositions();
}

void Model::AssignParticleNeighbourPositions()
{
	Particle *prtcl;

    for (int i = 0; i<nb_of_particles; i++)
    {
		
        prtcl = &particle_list[i];
        
        for (int j = 0; j < 12; j++)
        {
			if (prtcl->neighbour_list[j])
			{
				prtcl->neigpos[j][0] = prtcl->GetXDist(prtcl->neighbour_list[j]);
				prtcl->neigpos[j][1] = prtcl->neighbour_list[j]->pos[1] - prtcl->pos[1];
				prtcl->neigpos[j][2] = prtcl->GetZDist(prtcl->neighbour_list[j]);
			}
		}
		
        for (int j = 0; j < 6; j++)
        {
			if (prtcl->second_neighbour_list[j])
			{
				prtcl->secneigpos[j][0] = prtcl->GetXDist(prtcl->second_neighbour_list[j]);
				prtcl->secneigpos[j][1] = prtcl->second_neighbour_list[j]->pos[1] - prtcl->pos[1];
				prtcl->secneigpos[j][2] = prtcl->GetZDist(prtcl->second_neighbour_list[j]);
			}
		}
	}
}

void Model::MakeRepBox(bool model_definition_mode)
{
	int xint, yint, zint;
    Particle *tmp_prtcl;
	
   	/*****************************************
   	 * define dimensions of repulsion box
   	 * this needs to fit exactly the system size, 
   	 * and must not be larger as in previous versions
    *****************************************/
    
    SymmetricMatrix E(3);
    DiagonalMatrix D;
    
	Real inc = 2.0 * particle_radius;	

	Real height = xlength > zlength ? xlength : zlength;
	if (ylength > height)
		height = 2.0 * ylength;
	
	if (!model_definition_mode)	// normal case
	{
		// If viscoelastic, we need to redefine the rep_box spacing,
		// every time the rep_box is being rebuilt.
		// The new spacing is twice the largest particle-radius
		if (viscoelastic)
		{
			inc = 0.0;
			
			for (int i=0; i<nb_of_particles; i++)
    		{
				if (!particle_list[i].is_top_boundary && !particle_list[i].is_bottom_boundary)
				{
					E << particle_list[i].T * particle_list[i].T.t();
					EigenValues(E,D);
				
					if (sqrt(1.0 / D.element(0)) > inc)
						inc = sqrt(1.0 / D.element(0));
				}
    		}
			inc = inc * 2.0 * particle_radius;
    	}
		else	
			inc = 2.0 * particle_radius;
		
		// the OLD repbox has to be DELETED before we create a NEW one
		// this step is essential, other we get a MASSIVE memory leak
		for (int i=0; i<repbox_dim_x; i++)
		{
		    for (int j=0; j<repbox_dim_y; j++)
		    {
		        delete [] rep_box[i][j];
		    }
		}
		for (int i=0; i<repbox_dim_x; i++)
		{
			delete [] rep_box[i];
		}
		delete [] rep_box;
		
		// this definition is necessary so we dont have empty boxes in the vicinity of the
		// boundaries. Otherwise the size could be to small to catch particles which
		// which are shifted due to periodic boundaries
		repbox_dim_x = int ( xlength / (inc + xlength - wrapping_constant_x) );
		repbox_dim_y = int ( height / inc); // the box is not confined in y, so just to be on the save side
		repbox_dim_z = int ( zlength / (inc + zlength - wrapping_constant_z) );
	}
	else
	{
		
		// setting-up of the model, requires an increment of 2 * particle_radius
		repbox_dim_x = int ( xlength / inc );
		repbox_dim_y = int ( ylength / inc ); 
		repbox_dim_z = int ( zlength / inc ) ;		
	}
	
	for (int i=0; i<nb_of_particles; i++)
	{
		particle_list[i].repbox_dim_x = repbox_dim_x;
		particle_list[i].repbox_dim_y = repbox_dim_y;
		particle_list[i].repbox_dim_z = repbox_dim_z;
	}
	
	// every incx/y/z must be slightly larger than the particle-diameter,
	// but has to cover the system without gap
	repbox_inc_x = xlength / Real (repbox_dim_x);
	if (!model_definition_mode)
		repbox_inc_y = height / Real (repbox_dim_y);
	else 
		repbox_inc_y = (ylength) / Real (repbox_dim_y);
	repbox_inc_z = zlength / Real (repbox_dim_z);
		
   	/************************************
    make repulsion box
    ************************************/

    // note: this defines a 3-dimensional array and involves a somewhat tricky declaration and initialization of the
    // rep_box pointer. see e.g.: http://www.cpp-tutor.de/cpp/le11/le11_01.htm
    rep_box = new Particle***[repbox_dim_x];
    for (int i=0; i<repbox_dim_x; i++)
    {
        rep_box[i] = new Particle**[repbox_dim_y];
    }
    for (int i=0; i<repbox_dim_x; i++)
    {
        for (int j=0; j<repbox_dim_y; j++)
        {
            rep_box[i][j] = new Particle*[repbox_dim_z];
        }
    }
    
   	/************************************
    clear the repulsion box
    ************************************/
    
    for (int i = 0; i < repbox_dim_x; i++)
    {
        for (int j = 0; j < repbox_dim_y; j++)
        {
            for (int k = 0; k < repbox_dim_z; k++)
            {
                rep_box[i][j][k] = NULL;
            }
        }
    }

   	/************************************
    fill the repbox
    ************************************/

    // fill rep_box, one cell at a time
    for (int i=0; i<nb_of_particles; i++)
    {
	
		particle_list[i].pos[0] >=0.0 ? xint = int( particle_list[i].pos[0] / repbox_inc_x) : xint = int( particle_list[i].pos[0] / repbox_inc_x) - 1;		
	    if (xint < 0) xint += repbox_dim_x;
	    if (xint >= repbox_dim_x) xint -= repbox_dim_x;
		yint = int(particle_list[i].pos[1] / repbox_inc_y);					
		particle_list[i].pos[2] >= 0.0 ? zint = int( particle_list[i].pos[2] / repbox_inc_z) : zint = int( particle_list[i].pos[2] / repbox_inc_z) - 1;
	    if (zint < 0) zint += repbox_dim_z;
	    if (zint >= repbox_dim_z) zint -= repbox_dim_z;

        rep_box[xint][yint][zint] = &particle_list[i];
        particle_list[i].box_pos[0] = xint;
        particle_list[i].box_pos[1] = yint;
        particle_list[i].box_pos[2] = zint;
        
        particle_list[i].next_in_box = NULL;
		
    }

    // add tail particles
    for (int i=0; i<nb_of_particles; i++)
    {
    
		particle_list[i].pos[0] >=0.0 ? xint = int( particle_list[i].pos[0] / repbox_inc_x) : xint = int( particle_list[i].pos[0] / repbox_inc_x) - 1;		
	    if (xint < 0) xint += repbox_dim_x;
	    if (xint >= repbox_dim_x) xint -= repbox_dim_x;
		yint = int(particle_list[i].pos[1] / repbox_inc_y);					
		particle_list[i].pos[2] >= 0.0 ? zint = int( particle_list[i].pos[2] / repbox_inc_z) : zint = int( particle_list[i].pos[2] / repbox_inc_z) - 1;
	    if (zint < 0) zint += repbox_dim_z;
	    if (zint >= repbox_dim_z) zint -= repbox_dim_z;
	    
        if ( &particle_list[i] != rep_box[xint][yint][zint] )
        {
            tmp_prtcl = rep_box[xint][yint][zint];

            while (tmp_prtcl->next_in_box)
            {
                tmp_prtcl = tmp_prtcl->next_in_box;
            }

            tmp_prtcl->next_in_box = &particle_list[i];
        }
    }
}

void Model::DefineParticleNeighbours()
{

    Particle *list_same_plane[150];
    Particle *list_top[150];
    Particle *list_below[150];
    Particle *list_second_neighbours[550];
    Particle *tmp_list[550];

    /************************************
    search neighbours with repbox
    ************************************/

    int c1, c2, c3, c4, c5;
    Particle *prtcl, *neig;
    Real dist;

    for (int i=0; i<nb_of_particles; i++)
        particle_list[i].done = false;

    for (int i = 0; i<nb_of_particles; i++)
    {

        prtcl = &particle_list[i];

        c1 = 0;
        c2 = 0;
        c3 = 0;
        c4 = 0;
        c5 = 0;

        for (int i = -2; i <= 2; i++)
        {
            for (int j = -2; j <= 2; j++)
            {
                for (int k = -2; k <= 2; k++)
                {
                    if (prtcl->box_pos[0]+i >= 0 && prtcl->box_pos[1]+j >= 0 && prtcl->box_pos[2]+k >= 0)
                    {
						if (prtcl->box_pos[0]+i < repbox_dim_x && prtcl->box_pos[1]+j < repbox_dim_y && prtcl->box_pos[2]+k < repbox_dim_z)
						{
							if (rep_box[prtcl->box_pos[0]+i][prtcl->box_pos[1]+j][prtcl->box_pos[2]+k])
							{

								neig = rep_box[prtcl->box_pos[0]+i][prtcl->box_pos[1]+j][prtcl->box_pos[2]+k];

								while (neig)
								{
									if (!neig->done)
									{
										neig->done = true;
										tmp_list[c5++] = neig;

										// first neighbours
										if (neig->pos[1] > prtcl->pos[1]) // gleiche ebene
											list_top[c2++] = neig;
										else if (neig->pos[1] < prtcl->pos[1]) // tiefere ebene
											list_below[c3++] = neig;
										else // höhere ebene
											list_same_plane[c1++] = neig;

										dist = GetDistBetweenParticles(prtcl, neig);
										// second neighbours:
										if ( dist < 2.01 * second_neig_radius && dist > 1.99 * second_neig_radius )
											list_second_neighbours[c4++] = neig;

										if (neig->next_in_box)
											neig = neig->next_in_box;
										else
											neig = NULL;
									}
								}
							}
						}
                    }
                }
            }
        }

        AppendSamePlaneNeighbours(&list_same_plane[0], prtcl, c1);
        AppendTopPlaneNeighbours(&list_top[0], prtcl, c2);
        AppendBottomPlaneNeighbours(&list_below[0], prtcl, c3);

        AppendSecondNeighboursToList(&list_second_neighbours[0], prtcl, c4);

        for (int m=0; m<c5; m++)
        {
            tmp_list[m]->done = false;
        }
    }

}

void Model::MakePeriodicBoundariesOnXAxis()
{

    Real dist;

    Particle *list_same_plane[150];
    Particle *tmp_box_left[nb_of_particles];
    Particle *tmp_box_right[nb_of_particles];
    Particle *list_top[150];
    Particle *list_below[150];
    Particle *list_second_neighbours[550];

    int cleft = 0, cright = 0;
    int c1, c2, c3, c4;

    for ( int i = 0; i < nb_of_particles; i++ )
    {
        if (particle_list[i].is_left_boundary)
            tmp_box_left[cleft++] = &particle_list[i];
        else if (particle_list[i].is_right_boundary)
            tmp_box_right[cright++] = &particle_list[i];
    }

    for (int i=0; i<cleft; i++)
    {

        c1 = c2 = c3 = c4 = 0;

        for (int k=0; k < cright; k++)
        {

            dist = GetDistBetweenParticles( tmp_box_left[i], tmp_box_right[k] );

            if (dist > 1.99 * particle_radius && dist < 2.01 * particle_radius)
            {

                if (tmp_box_right[k]->pos[1] > tmp_box_left[i]->pos[1] + 0.1*particle_radius)
                {
                    list_top[c1++] = tmp_box_right[k];
                }
                else if (tmp_box_right[k]->pos[1] < tmp_box_left[i]->pos[1] - 0.1*particle_radius)
                {
                    list_below[c2++] = tmp_box_right[k];
                }
                else
                {
                    list_same_plane[c3++] = tmp_box_right[k];
                }
            }

            if (dist > 1.9 * second_neig_radius && dist < 2.1 * second_neig_radius)
            {
                list_second_neighbours[c4++] = tmp_box_right[k];
            }
        }

        AppendSamePlaneNeighbours( &list_same_plane[0], tmp_box_left[i], c3 );
        AppendBottomPlaneNeighbours( &list_below[0], tmp_box_left[i], c2 );
        AppendTopPlaneNeighbours( &list_top[0], tmp_box_left[i], c1 );

        AppendSecondNeighboursToList(&list_second_neighbours[0], tmp_box_left[i], c4);
    }

    for (int i=0; i<cright; i++)
    {

        c1 = c2 = c3 = c4 = 0;

        for (int k=0; k < cleft; k++)
        {

            dist = GetDistBetweenParticles( tmp_box_right[i], tmp_box_left[k] );

            if (dist > 1.99 * particle_radius && dist < 2.01 * particle_radius)
            {

                if (tmp_box_left[k]->pos[1] > tmp_box_right[i]->pos[1] + 0.1*particle_radius)
                {
                    list_top[c1++] = tmp_box_left[k];
                }
                else if (tmp_box_left[k]->pos[1] < tmp_box_right[i]->pos[1] - 0.1*particle_radius)
                {
                    list_below[c2++] = tmp_box_left[k];
                }
                else
                {
                    list_same_plane[c3++] = tmp_box_left[k];
                }
            }

            if (dist > 1.99 * second_neig_radius && dist < 2.01 * second_neig_radius)
            {
                list_second_neighbours[c4++] = tmp_box_left[k];
            }
        }

        AppendSamePlaneNeighbours( &list_same_plane[0], tmp_box_right[i], c3 );
        AppendBottomPlaneNeighbours( &list_below[0], tmp_box_right[i], c2 );
        AppendTopPlaneNeighbours( &list_top[0], tmp_box_right[i], c1 );

        AppendSecondNeighboursToList(&list_second_neighbours[0], tmp_box_right[i], c4);
    }
    
}

void Model::MakePeriodicBoundariesOnZAxis()
{
    Real dist;

    Particle *list_same_plane[150];
    Particle *tmp_box_front[nb_of_particles];
    Particle *tmp_box_back[nb_of_particles];
    Particle *list_top[150];
    Particle *list_below[150];
    Particle *list_second_neighbours[550];

    int cfront = 0, cback = 0;
    int c1, c2, c3, c4;

    for ( int i = 0; i < nb_of_particles; i++ )
    {
        if (particle_list[i].is_back_boundary)
            tmp_box_back[cback++] = &particle_list[i];
        else if (particle_list[i].is_front_boundary)
            tmp_box_front[cfront++] = &particle_list[i];
    }
    
    for (int i=0; i<cback; i++)
    {

        c1 = c2 = c3 = c4 = 0;

        for (int k=0; k < cfront; k++)
        {

			dist = GetDistBetweenParticles( tmp_box_back[i], tmp_box_front[k]);
			
            if (dist > 1.99 * particle_radius && dist < 2.01 * particle_radius)
            {

                if (tmp_box_front[k]->pos[1] > tmp_box_back[i]->pos[1] + 0.01*particle_radius)
                {
                    list_top[c1++] = tmp_box_front[k];
                }
                else if (tmp_box_front[k]->pos[1] < tmp_box_back[i]->pos[1] - 0.01*particle_radius)
                {
                    list_below[c2++] = tmp_box_front[k];
                }
                else
                {
                    list_same_plane[c3++] = tmp_box_front[k];
                }
            }

            if (dist > 1.99 * second_neig_radius && dist < 2.01 * second_neig_radius)
            {
                list_second_neighbours[c4++] = tmp_box_front[k];
            }
        }
    
        AppendSamePlaneNeighbours( &list_same_plane[0], tmp_box_back[i], c3 );
        AppendBottomPlaneNeighbours( &list_below[0], tmp_box_back[i], c2 );
        AppendTopPlaneNeighbours( &list_top[0], tmp_box_back[i], c1 );

        AppendSecondNeighboursToList(&list_second_neighbours[0], tmp_box_back[i], c4);
    }

    for (int i=0; i<cfront; i++)
    {

        c1 = c2 = c3 = c4 = 0;

        for (int k=0; k < cback; k++)
        {

            dist = GetDistBetweenParticles( tmp_box_front[i], tmp_box_back[k] );

            if (dist > 1.99 * particle_radius && dist < 2.01 * particle_radius )
            {

                if (tmp_box_back[k]->pos[1] > tmp_box_front[i]->pos[1] + 0.01*particle_radius )
                {
                    list_top[c1++] = tmp_box_back[k];
                }
                else if (tmp_box_back[k]->pos[1] < tmp_box_front[i]->pos[1] - 0.01*particle_radius )
                {
                    list_below[c2++] = tmp_box_back[k];
                }
                else
                {
                    list_same_plane[c3++] = tmp_box_back[k];
                }
            }

            if (dist > 1.99 * second_neig_radius && dist < 2.01 * second_neig_radius)
            {
                
                list_second_neighbours[c4++] = tmp_box_back[k];
            }
        }
        
        AppendSamePlaneNeighbours( &list_same_plane[0], tmp_box_front[i], c3 );
        AppendBottomPlaneNeighbours( &list_below[0], tmp_box_front[i], c2 );
        AppendTopPlaneNeighbours( &list_top[0], tmp_box_front[i], c1 );

        AppendSecondNeighboursToList(&list_second_neighbours[0], tmp_box_front[i], c4);
        
    }
}

void Model::ChangeBox(Particle *tmp_prtcl)
{
	if (tmp_prtcl)
	{
		Particle *preceding_particle;

		int xint, yint, zint;
		
		tmp_prtcl->pos[0] >=0.0 ? xint = int( tmp_prtcl->pos[0] / repbox_inc_x) : xint = int( tmp_prtcl->pos[0] / repbox_inc_x) - 1;		
	    if (xint < 0) xint += repbox_dim_x;
	    if (xint >= repbox_dim_x) xint -= repbox_dim_x;
		yint = int(tmp_prtcl->pos[1] / repbox_inc_y);
		tmp_prtcl->pos[2] >= 0.0 ? zint = int( tmp_prtcl->pos[2] / repbox_inc_z) : zint = int( tmp_prtcl->pos[2] / repbox_inc_z) - 1;
	    if (zint < 0) zint += repbox_dim_z;
	    if (zint >= repbox_dim_z) zint -= repbox_dim_z;
	    
		// only if it is necessary at all to change the box
		if ( (tmp_prtcl->box_pos[0] != xint) || (tmp_prtcl->box_pos[1] != yint) || (tmp_prtcl->box_pos[2] != zint))
		{
			
		   #pragma omp critical
			{

				// find and connect the particles in the current next_in_box-chain, which follow and precede tmp_prtcl
				if (rep_box[tmp_prtcl->box_pos[0]][tmp_prtcl->box_pos[1]][tmp_prtcl->box_pos[2]] != tmp_prtcl)
				{
					preceding_particle = rep_box[tmp_prtcl->box_pos[0]][tmp_prtcl->box_pos[1]][tmp_prtcl->box_pos[2]];
					
					while (preceding_particle->next_in_box != tmp_prtcl)
						preceding_particle = preceding_particle->next_in_box;
					if (tmp_prtcl->next_in_box)
						preceding_particle->next_in_box = tmp_prtcl->next_in_box;
					else
						preceding_particle->next_in_box = NULL;
				}
				else
				{
					if (tmp_prtcl->next_in_box)
						rep_box[tmp_prtcl->box_pos[0]][tmp_prtcl->box_pos[1]][tmp_prtcl->box_pos[2]] = tmp_prtcl->next_in_box;
					else
						rep_box[tmp_prtcl->box_pos[0]][tmp_prtcl->box_pos[1]][tmp_prtcl->box_pos[2]] = NULL;
				}

				// will be appended to a new list, thus hasn't got a "next_in_box"-neighbour yet
				tmp_prtcl->next_in_box = NULL;

				// put tmp_prtcl into its new box by appending it to the last particle in the chain of that box
				if (rep_box[xint][yint][zint])
				{
					preceding_particle = rep_box[xint][yint][zint];
					while (preceding_particle->next_in_box)
					{
						preceding_particle = preceding_particle->next_in_box;
					}
					
					preceding_particle->next_in_box = tmp_prtcl;
				}
				else
				{
					rep_box[xint][yint][zint] = tmp_prtcl;
				}

				// update info stored in particle
				tmp_prtcl->box_pos[0] = xint;
				tmp_prtcl->box_pos[1] = yint;
				tmp_prtcl->box_pos[2] = zint;
			}
		}
	}
	else
	{
		cout << "something bad happend in the ChangeBox funtion" << endl;
		cout << "however, I continue and try to work around the problem" << endl;
		cout << "you SHOULD check the Model::ChangeBox-Function though" << endl;
		
		DumpVTKFile("changebox-error.vtu");
		
		MakeRepBox();
	}
}

void Model::AppendSamePlaneNeighbours(Particle **list, Particle *curpar, int listsize)
{

    Real dist, angle;
    Particle *first_neighbours[6] = {0};
    int counter = 0;

    // find the first neighbours
    for (int i = 0; i<listsize; i++)
    {
        dist = GetDistBetweenParticles(curpar, list[i]);

        if ( dist < 2.01*particle_radius && dist > 1.99*particle_radius )
        {
            first_neighbours[counter] = list[i];
            counter++;
        }
    }



    // calc angle from x-axis (array element will be positive if a neighbour is present)
    for ( int i = 0; i<counter; i++ )
    {
        angle = GetAngleFromPositiveXAxis(curpar, first_neighbours[i]) + 0.2;
        if (angle > 6.0) angle = 0.2; // corrects imprecisions regarding spring 0
        //angle -= 0.2;

        curpar->neighbour_list[int(angle / 1.0471976) + 3] = first_neighbours[i];
    }

}

void Model::AppendTopPlaneNeighbours(Particle **list, Particle *curpar, int listsize)
{

    Real dist, angle;
    Particle *first_neighbours[3] = {0};
    int counter = 0;

    // find the first neighbours
    for (int i = 0; i<listsize; i++)
    {
        dist = GetDistBetweenParticles(curpar, list[i]);

        if ( dist < 2.01*particle_radius && dist > 1.99*particle_radius )
        {
            first_neighbours[counter] = list[i];
            counter++;
        }
    }

    // calc angle from x-axis (array element will be positive if a neighbour is present)
    for ( int i = 0; i<counter; i++ )
    {
        angle = GetAngleFromPositiveXAxis(curpar, first_neighbours[i]);
        if (angle > 6.0) angle = 0.0; // corrects imprecisions regarding spring 0

        curpar->neighbour_list[int(angle / 2.0943951) + 9] = first_neighbours[i];
    }

}

void Model::AppendSecondNeighboursToList(Particle **list, Particle *curpar, int listsize)
{
    
    Particle *toplist[3], *bottomlist[3];
    int toplistsize = 0, bottomlistsize = 0;
    Real angle;
    
    for ( int i = 0; i<listsize; i++ )
    {
		if (list[i])
		{
			if(list[i]->pos[1] > curpar->pos[1])
			{
				toplist[toplistsize++] = list[i];
			}
			else
			{
				bottomlist[bottomlistsize++] = list[i];
			}
		}
	}
    
	// calc angle from x-axis (array element will be positive if a neighbour is present)
    for ( int i = 0; i<bottomlistsize; i++ )
    {
        angle = GetAngleFromPositiveXAxis(curpar, bottomlist[i]);
        //if (angle > 6.0) angle = 0.0; // corrects imprecisions regarding spring 0

        curpar->second_neighbour_list[int(angle / 2.0943951)] = bottomlist[i];

    }
    
	// calc angle from x-axis (array element will be positive if a neighbour is present)
    for ( int i = 0; i<toplistsize; i++ )
    {
        angle = GetAngleFromPositiveXAxis(curpar, toplist[i]);
        if (angle > 6.0) angle = 0.0; // corrects imprecisions regarding spring 0

        curpar->second_neighbour_list[int(angle / 2.0943951) + 3] = toplist[i];
    }
    
}

void Model::AppendBottomPlaneNeighbours(Particle **list, Particle *curpar, int listsize)
{

    Real dist, angle;
    Particle *first_neighbours[3] = {0};
    int counter = 0;

    // find the first neighbours
    for (int i = 0; i<listsize; i++)
    {
        dist = GetDistBetweenParticles(curpar, list[i]);

        if ( dist < 2.01*particle_radius && dist > 1.99*particle_radius )
        {
            first_neighbours[counter] = list[i];
            counter++;
        }
    }

    // calc angle from x-axis (array element will be positive if a neighbour is present)
    for ( int i = 0; i<counter; i++ )
    {
        angle = GetAngleFromPositiveXAxis(curpar, first_neighbours[i]);
        if (angle > 6.0) angle = 0.0; // corrects imprecisions regarding spring 0

        curpar->neighbour_list[int(angle / 2.0943951)] = first_neighbours[i];
    }

}

Real Model::GetDistBetweenParticles(Particle *curpar, Particle *neig)
{
    Real x, y, z;

    x = neig->GetXDist(curpar);
    y = curpar->pos[1] - neig->pos[1];
    z = neig->GetZDist(curpar);

    return sqrt(x*x + y*y + z*z);
}

Real Model::GetAngleFromPositiveXAxis(Particle *curpar, Particle *neig)
{
    // in-plane, thus we dont need y (==height)
    Real dx, dz, ag;

    dx = -(neig->GetXDist(curpar));
    dz = -(neig->GetZDist(curpar));

	ag = acos( dx / sqrt(dx*dx + dz*dz) );

    if ( dz > 0 )
        ag = (2.0*3.14159265) - ag;

    return ag;
}

void Model::CalcSizeAndNbOfParticlesAndSystemDimensions(Real xsize, Real ysize, Real zsize, int x_particles)
{
    nb_of_particles_in_x = x_particles;
    particle_radius = xsize / (2.0 * nb_of_particles_in_x + 1.0);
    second_neig_radius = sqrt(2.0) * particle_radius;

    nb_of_particles_in_z = int ((zsize / particle_radius - 2.0 - 2.0/sqrt(3.0)) / sqrt(3.0)) + 1;
    //must be even:
    if (nb_of_particles_in_z % 2 != 0)
        nb_of_particles_in_z -= 1;

    nb_of_particles_in_y = int(ysize / (particle_radius*2.0*sqrt(2.0)/sqrt(3.0)));

    nb_of_particles = nb_of_particles_in_x * nb_of_particles_in_y * nb_of_particles_in_z;

    xlength = xsize;
    ylength = nb_of_particles_in_y * particle_radius * 2.0 * sqrt(2.0)/sqrt(3.0);
    zlength = (nb_of_particles_in_z - 1) * sqrt(3.0) * particle_radius + 2.0 * particle_radius + 2.0 * (particle_radius / sqrt(3.0));

    // these are used to calculate the dsitance of particles at different boundaries
    // this differs slightly from the "real" system-size, and are a "would-be-distance"
    wrapping_constant_x = nb_of_particles_in_x * 2.0 * particle_radius;
	wrapping_constant_z = nb_of_particles_in_z * particle_radius * sqrt(3.0);
	
}

void Model::MakeHexagonalLattice(Real xsize, Real ysize, Real zsize)
{

    int i , j , k;

    for (i=0; i<nb_of_particles_in_y; i++)
    {
        for (j=0; j<nb_of_particles_in_z; j++)
        {
            for (k=0; k<nb_of_particles_in_x; k++)
            {

                particle_list[k + j * (nb_of_particles_in_x) + i * (nb_of_particles_in_z * nb_of_particles_in_x)].pos[0] = k * 2.0 * particle_radius + particle_radius;
                particle_list[k + j * (nb_of_particles_in_x) + i * (nb_of_particles_in_z * nb_of_particles_in_x)].pos[1] = i * 2.0 * sqrt(2.0) / sqrt(3.0) * particle_radius + particle_radius;
                particle_list[k + j * (nb_of_particles_in_x) + i * (nb_of_particles_in_z * nb_of_particles_in_x)].pos[2] = j * sqrt(3.0) * particle_radius + particle_radius;

                if (j%2 == 0)
                {
                    particle_list[k + j * (nb_of_particles_in_x) + i * (nb_of_particles_in_z * nb_of_particles_in_x)].pos[0] += particle_radius;
                }

                if (i%2 == 0)
                {
                    particle_list[k + j * (nb_of_particles_in_x) + i * (nb_of_particles_in_z * nb_of_particles_in_x)].pos[2] += 2.0 * (particle_radius / sqrt(3.0));
                }
            }
        }
    }
}

void Model::MarkNextAndSecondNextBoundaryParticles()
{
	int i,j,k;

    for (i=0; i<nb_of_particles; i++)
    {
        for (j=0; j<12; j++)
        {
            if (!(particle_list[i].neighbour_list[j]))
            {
                particle_list[i].is_boundary = true;
                break;
			}
		}
    }
    if(is_second_neighbour_model)
    {
		for (i=0; i<nb_of_particles; i++)
		{
		    for (j=0; j<6; j++)
		    {
		        if (!(particle_list[i].second_neighbour_list[j]))
		        {
		            particle_list[i].is_boundary = true;
		            break;
		        }
		    }
		}
    }
    

    for (i=0; i<nb_of_particles; i++)
    {
        if (particle_list[i].is_boundary)
        {
            for (j=0; j<12; j++)
            {
                if (particle_list[i].neighbour_list[j])
                {
                    particle_list[i].no_break[j] = true;

                    for (k=0; k<12; k++)
                    {
                        if (particle_list[i].neighbour_list[j]->neighbour_list[k])
                        {
                            if (particle_list[i].neighbour_list[j]->neighbour_list[k] == &particle_list[i])
                            {
                                particle_list[i].neighbour_list[j]->no_break[k] = true;
                            }
                        }
                    }
                }
            }
           	if(is_second_neighbour_model)
    		{
		        for (j=0; j<6; j++)
		        {
		            if (particle_list[i].second_neighbour_list[j])
		            {
		                particle_list[i].second_neigh_no_break[j] = true;

		                for (k=0; k<6; k++)
		                {
		                    if (particle_list[i].second_neighbour_list[j]->second_neighbour_list[k])
		                    {
		                        if (particle_list[i].second_neighbour_list[j]->second_neighbour_list[k] == &particle_list[i])
		                        {
		                            particle_list[i].second_neighbour_list[j]->second_neigh_no_break[k] = true;
		                        }
		                    }
		                }
		            }
		        }
            }
        }
    }
}

void Model::MarkTopBottomLeftRightFrontBackNextNeighbourBoundaryParticles()
{
	int i;
	
	for (i=0; i<nb_of_particles; i++)
	{
		// bottom-boundary?
		particle_list[i].is_bottom_boundary = false;
		if ((!particle_list[i].neighbour_list[0]) && (!particle_list[i].neighbour_list[1]) && (!particle_list[i].neighbour_list[2]))
		{
			particle_list[i].is_bottom_boundary = true;
		}

		// top-boundary?
		particle_list[i].is_top_boundary = false;
		if ((!particle_list[i].neighbour_list[9]) && (!particle_list[i].neighbour_list[10]) && (!particle_list[i].neighbour_list[11]))
		{
			particle_list[i].is_top_boundary = true;
		}
		// front-boundary?
		particle_list[i].is_front_boundary = false;
		if ( !particle_list[i].neighbour_list[7] && !particle_list[i].neighbour_list[8])
		{
			particle_list[i].is_front_boundary = true;
		}
		// back-boundary?
		particle_list[i].is_back_boundary = false;
		if ((!particle_list[i].neighbour_list[4]) && (!particle_list[i].neighbour_list[5]))
		{
			particle_list[i].is_back_boundary = true;
		}
		// left-boundary?
		particle_list[i].is_left_boundary = false;
		if ((!particle_list[i].neighbour_list[6]))
		{
			particle_list[i].is_left_boundary = true;
		}
		// right-boundary?
		particle_list[i].is_right_boundary = false;
		if ((!particle_list[i].neighbour_list[3]))
		{
			particle_list[i].is_right_boundary = true;
		}
	}
}

void Model::MarkTopBottomLeftRightFrontBackSecondNextNeighbourBoundaryParticles()
{
	
	int i, j;
	Particle *p;
	Particle *list[nb_of_particles];
	int c = 0;
	
	/* 	
	 * 	Unfortunately the search for second-next-neighbour-boundaries
	 * 	is much less straightforward than the search for first-neighbour-boundaries.
	 * 	Fortunately, however, this isn't necessary in the x and the y-direction.
	 *
	 * 	In y: because missing next- and second-next-neighbours are on the same level
	 * 	In x: same reason, relevant next- and second-neighbours are on a line.
	 * 	
	 * 	We have to look only in z ("front" and "back" of the system).
	 * 	There used to be an error were due to the geometry of the left system boundary
	 * 	some particles were mistaken for back- and front- boundaries.
	 * 	Therefor the pos[2]-query.
	 */
	
	c = 0;
	for (i=0; i<nb_of_particles; i++)
	{
		if (particle_list[i].is_back_boundary)
		{
			for (j=0; j<12; j++)
			{
				if (particle_list[i].neighbour_list[j])
				{
					if (particle_list[i].neighbour_list[j]->is_boundary)
					{
						p = particle_list[i].neighbour_list[j];
						
						if (!p->is_back_boundary)
						{
							if (!p->second_neighbour_list[0] && !p->second_neighbour_list[3])
								// workaround for front-particles which are missing the same springs as above but are situated elsewhere
								if ( p->pos[2] < 1.1*particle_radius + sqrt(3.0)*particle_radius )
									list[c++] = p;
						}
					}
				}
			}
		}
	}
	for (i=0; i<c; i++)
		list[i]->is_back_boundary = true;
	
	c = 0;
	for (i=0; i<nb_of_particles; i++)
	{
		if (particle_list[i].is_front_boundary)
		{
			for (j=0; j<12; j++)
			{
				if (particle_list[i].neighbour_list[j])
				{
					if (particle_list[i].neighbour_list[j]->is_boundary)
					{
						p = particle_list[i].neighbour_list[j];
						
						if (!p->is_front_boundary)
						{
							if (!p->second_neighbour_list[2] && !p->second_neighbour_list[5])
								// workaround for front-particles which are missing the same springs as above but are situated elsewhere
								if ( p->pos[2] > zlength - (1.1*particle_radius + sqrt(3.0)*particle_radius) )
									list[c++] = p;
						}
					}
				}
			}
		}					
	}
	for (i=0; i<c; i++)
		list[i]->is_front_boundary = true;	

}

void Model::FixBoundaryParticles()
{
	for (int i=0; i < nb_of_particles; i++)
	{
		if (particle_list[i].is_boundary)
		{
			if (particle_list[i].is_top_boundary || particle_list[i].is_bottom_boundary)
			{
				particle_list[i].fix_y = true;
			}
			if (particle_list[i].is_left_boundary || particle_list[i].is_right_boundary)
			{
				particle_list[i].fix_x = true;
			}
			if (particle_list[i].is_front_boundary || particle_list[i].is_back_boundary)
			{
				particle_list[i].fix_z = true;				
			}
		}
	}
}

void Model::Info()
{
	cout << endl;
	cout << "internal model settings:" << endl << endl;
	cout << "   Resolution (in x-direction): " << nb_of_particles_in_x << endl;
	cout << "   Number of particles: " << nb_of_particles << endl;
	cout << endl;
	cout << "   xlength: " << xlength << endl;
	cout << "   ylength: " << ylength << endl;
	cout << "   zlength: " << zlength << endl;
	cout << endl;
	
	if (is_second_neighbour_model)
		cout << "   Second neighbour lattice" << endl;
	else 
		cout << "   Granular material: only repulsion but no springs are considered" << endl;

	if (is_second_neighbour_model)
	{
		if (repulsion)
			cout << "   Repulsion of unconnected particles is considered" << endl;
		else
			cout << "   Repulsion of unconnected particles is NOT considered" << endl;
	}
	
	cout << endl;

	if (viscoelastic)
		cout << "   Model is viscoelastic" << endl << endl;
	else
		cout << "   Model is NOT viscoelastic" << endl << endl;

	if (use_periodic_boundaries_in_x)
		cout << "   Periodic boundaries in X-direction" << endl;
	else
		cout << "   Normal (non-periodic) boundaries in X-direction" << endl;
		
	if (use_periodic_boundaries_in_z)
		cout << "   Periodic boundaries in Z-direction" << endl;
	else 
		cout << "   Normal (non-periodic) boundaries in Z-direction" << endl;
	cout << endl;
	
	if (walls_in_x)
		cout << "   Walls in X-direction" << endl;
	else 
		cout << "   NO Walls in X-direction" << endl;

	if (walls_in_z)
		cout << "   Walls in Z-direction" << endl;
	else 
		cout << "   NO Walls in Z-direction" << endl;
	cout << endl;

	if (mohr_coulomb_failure)
		cout << "   SHEAR FAILURE (Mohr-Coulomb type) is ACTIVATED" << endl;
	else
		cout << "   SHEAR FAILURE is NOT activated" << endl;
	cout << endl;
	
	cout << "corresponding natural parameters:" << endl << endl;
	cout << "   Scaling factor: " << scale_factor << endl;
	cout << "   Dimensions (x, y, z in meters): " << xlength*scale_factor << ", " << ylength*scale_factor << ", " << zlength*scale_factor << endl;
	cout << endl;	
}

void Model::ActivateGravity()
{
	bool cond;
	Particle *n;
	
	// first "free" the particles at the top of the model and let them break
	for (int i=0; i<nb_of_particles; i++)
	{
		cond = false;
		
		if (particle_list[i].is_top_boundary)
		{
			cond = true;			
			particle_list[i].fix_y = false;
		}
		
		//only if nothing else:
		if (cond)
		{
			if (particle_list[i].is_left_boundary || particle_list[i].is_right_boundary)
				cond = false;

			if (particle_list[i].is_front_boundary || particle_list[i].is_back_boundary)
				cond = false;
		}

		if (cond)
		{
			
			particle_list[i].is_boundary = false;
            
			for (int j=0; j<12; j++)
			{
				if (particle_list[i].neighbour_list[j])
				{
					n = particle_list[i].neighbour_list[j];
					
					if (!n->is_left_boundary && !n->is_right_boundary)
					{
						if (!n->is_front_boundary && !n->is_back_boundary)
						{
							particle_list[i].no_break[j] = false;
						}
					}
				}
			}
			
			for (int j=0; j<6; j++)
			{
				if (particle_list[i].second_neighbour_list[j])
				{
					n = particle_list[i].second_neighbour_list[j];
					
					if (!n->is_left_boundary && !n->is_right_boundary)
					{
						if (!n->is_front_boundary && !n->is_back_boundary)
						{
							particle_list[i].second_neigh_no_break[j] = false;
						}
					}
				}
			}
		}
	}
	
	Real scaled_stress = 0.0;
	use_gravity_force = true;
	Real stress_scale_factor = 5.0/7.0;

	// below I'm using an abridged version of the gravity-force equation
	// this minimizes numerical errors
	for (int i=0; i<nb_of_particles; i++)
	{
		particle_list[i].use_gravity_force = true;

		stress_scale_factor = - (((particle_list[i].v+1)*(2*particle_list[i].v-1)) / ((2*particle_list[i].v*particle_list[i].v-particle_list[i].v+1)*particle_list[i].young));

		scaled_stress = sqrt(6) * sqrt(3) * pow(particle_list[i].radius, 3) * scale_factor * particle_list[i].density * 9.81;
	
		scaled_stress = scaled_stress * particle_list[i].young / particle_list[i].real_young;

		scaled_stress = scaled_stress / (pi * particle_list[i].radius*particle_list[i].radius);
		scaled_stress = scaled_stress * ( pi * particle_list[i].radius*particle_list[i].radius );

		scaled_stress = scaled_stress * stress_scale_factor;
		// the factor below is due to the spacefilling and considers finally a
		// hexagonal instead of circular section of real particles
		scaled_stress = scaled_stress * 0.9069;
		
		particle_list[i].fg = scaled_stress;
	}
}

Real Model::Average_mean_stress()
{
	Real avx = 0.0;
	
	for (int i=0; i<nb_of_particles; i++)
		if (!particle_list[i].is_boundary)
			avx += particle_list[i].mean_stress;
		
	return (avx/nb_of_particles);
} 

void Model::Activate_Mohr_Coulomb_Failure(bool activate)
{
	Model::mohr_coulomb_failure = activate;
}

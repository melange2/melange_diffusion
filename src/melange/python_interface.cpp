/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/
    
#include "model.h"
#include "experiment_settings.h"
#include "deformation.h"
#include "relaxation.h"

#include <boost/python.hpp>
#include <boost/python/def.hpp>

using namespace boost::python;

BOOST_PYTHON_MODULE(melange)
{
    class_<Deformation>("Deformation", init < Model* >() )
		// the functions in the block below change the wrapping constant only in the principal extension-direction
		// void DeformLattice(Real x = 0.01, Real z = 0.0)
		.def("DeformLattice", &Deformation::DeformLattice, DeformLattice_overloads())
		// void DeformLatticeXMid(Real x, Real height, Real left, Real right);
		.def("DeformLatticeXMid", &Deformation::DeformLatticeXMid)
		// void Deformation::DeformLatticeSandbox(Real x, Real height, Real left, Real right)
		// in this example the height marks the MAXIMUM height for localized stretching (in difference to DeformLatticeXMid)
		.def("DeformLatticeSandbox", &Deformation::DeformLatticeSandbox)
		// void Deformation::DeformLatticeSimpleShear(Real z)
		.def("DeformLatticeSimpleShear", &Deformation::DeformLatticeSimpleShear)
		// void Deformation::VerticallyIncreasingXStrain(Real maxx, Real min_height, Real max_height)
		.def("VerticallyIncreasingXStrain", &Deformation::VerticallyIncreasingXStrain, VerticallyIncreasingXStrain_overloads())
		// void DeformLatticeWingCrack(Real y);
		.def("DeformLatticeWingCrack", &Deformation::DeformLatticeWingCrack)
		// void DeformLatticeXMid2(Real x, Real height, Real left1, Real right1, Real left2, Real right2)
		.def("DeformLatticeXMid2", &Deformation::DeformLatticeXMid2)
        ;

    class_<Relaxation>("Relaxation", init < Model* >() )
        .def("Relax", &Relaxation::Relax)
        .def("SetRelaxationThreshold", &Relaxation::SetRelaxationThreshold)
        .def("Info", &Relaxation::Info)
        // sets the factor for the overrelaxation. Default value (set in constructor of relaxation-class) is 1.8
        .def("SetOverRelaxationFactor", &Relaxation::SetOverRelaxationFactor)
        // ActivateDynamicORF(bool activate)
        .def("ActivateDynamicORF", &Relaxation::ActivateDynamicORF)
        ;

    class_<ExperimentSettings>( "ExperimentSettings", init < Model* >() )
		.def("SetGaussianStrengthDistribution", &ExperimentSettings::SetGaussianStrengthDistribution)
		//ExperimentSettings::SetGaussianYoungDistribution ( Real g_mean, Real g_sigma )
		.def("SetGaussianYoungandVDistribution", &ExperimentSettings::SetGaussianYoungandVDistribution)

		// if gravity shall be included
		.def("ActivateGravity", &ExperimentSettings::ActivateGravity)

		// elastic parameters
		// this resets a new (real) standard-youngs-modulus for every particle as given and
		// resets the youngs modulus on model-level to 1
		// also resets for the springk's 
		.def("SetStandardYoungsModulusAndV", &ExperimentSettings::SetStandardYoungsModulusAndV)
		.def("SetScalingFactor", &ExperimentSettings::SetScalingFactor)
		.def("SetDensityForAllParticles", &ExperimentSettings::SetDensityForAllParticles)
		//void ExperimentSettings::SetDensity(Real density, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2)
		.def("SetDensity", &ExperimentSettings::SetDensity)
		// void SetRealYoungAndV(Real real_young, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2);
		// as usual, x1 < x2, y1 < y2, etc.
		.def("SetRealYoungAndV", &ExperimentSettings::SetRealYoungAndV)

		// sets a no-break condition for all the particles below a certain threshold
		// the no_break is set for 1st- and second neighbour particles
		// argument: a Real
		.def("SetNoBreakY", &ExperimentSettings::SetNoBreakY)
		// sets a no-break condition for all the particles in x between 2 values:
		// ExperimentSettings::SetNoBreakX(Real left, Real right)
		.def("SetNoBreakX", &ExperimentSettings::SetNoBreakX)
		// resets the nobreak-condition previously set
		// void ResetNoBreak();
		.def("ResetNoBreak", &ExperimentSettings::ResetNoBreak)

		// Cuts the springs (1st and 2nd neighbour) which cross a predefined plane on a
		// certain x-position, normal to the x-axis.
		// ExperimentSettings::SeparateParticlesCrossingDistinctYZPlane(Real planex, Real plane_upper_y, Real plane_lower_y, Real plane_front_z, Real plane_back_z)
		.def("SeparateParticlesCrossingDistinctYZPlane", &ExperimentSettings::SeparateParticlesCrossingDistinctYZPlane)
		// similar function to the one aboe, but "weakens" springs crossing a plane
		// void ExperimentSettings::WeakenSpringsCrossingDistinctYZPlane(young_factor, break_factor, Real planex, Real plane_upper_y, Real plane_lower_y, Real plane_front_z, Real plane_back_z)
		.def("WeakenSpringsCrossingDistinctYZPlane", &ExperimentSettings::WeakenSpringsCrossingDistinctYZPlane)
		// Severes springs crossing an arbitrarily positioned plane.
		// It's defined by the upper- and lower x,y,z-boundary and affects the points therein
		// Plane itself is defined by 3 points in X/Y/Z
		// void SeverSpringsCrossingPlane( Real p_1_x, Real p_1_y, Real p_1_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z, Real xlimit1, Real xlimit2, Real ylimit1, Real ylimit2, Real zlimit1, Real zlimit2);
		.def("SeverSpringsCrossingPlane", &ExperimentSettings::SeverSpringsCrossingPlane)
		// Severes springs crossing an arbitrarily positioned triangular area
		// ExperimentSettings::SeverSpringsCrossingTriangle( Real p_1_x, Real p_1_y, Real p_1_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z )
		.def("SeverSpringsCrossingTriangle", &ExperimentSettings::SeverSpringsCrossingTriangle)
		// Severes springs crossing an arbitrarily positioned circular area. The orientation of the plane is defined via the center point and 2 arbitrary points on the plane. The extend of the circle is defined by the center and the radius.
		// void SeverSpringsCrossingCircularPlane( Real radius, Real center_x, Real center_y, Real center_z, Real p_2_x, Real p_2_y, Real p_2_z, Real p_3_x, Real p_3_y, Real p_3_z );
		.def("SeverSpringsCrossingCircularPlane", &ExperimentSettings::SeverSpringsCrossingCircularPlane)

		// does as it says: the breaking-strength to a certain value in the given cuboidal domain
		// Breaking strength is the "real" breaking strength in Pa, standard is 1 MPa.
		// void ChangeBreakingStrength(Real breaking_strain, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax);
		.def("ChangeBreakingStrength", &ExperimentSettings::ChangeBreakingStrength)
		// enables the breaking of bonds along the walls in z
		// void ExperimentSettings::EnableSideWallBreakingInZ()
		// enables the breaking of bonds along the walls in z
		.def("EnableSideWallBreakingInZ", &ExperimentSettings::EnableSideWallBreakingInZ)
		.def("EnableSideWallBreakingInZAndX", &ExperimentSettings::EnableSideWallBreakingInZAndX)

		// void Unfix_x() / Unfix_y() / Unfix_z()
		// the boundary particles are usually fixed in one direction or another
		// these functions unfix them again
		.def("Unfix_z", &ExperimentSettings::Unfix_z)
		// void Unfix_y()
		.def("Unfix_y", &ExperimentSettings::Unfix_y)
		// void Unfix_x()
		.def("Unfix_x", &ExperimentSettings::Unfix_x)

		// more control over the boundary particles: this fixes these particles COMPLETELY during relaxation
		// useful eg in combination with gravity-forces (Pin_Bottom_Boundary) or
		// to build some comtainment shape (eg a chest, see examples)
		// void Pin_Back_Boundary();
		.def ("Pin_Back_Boundary", &ExperimentSettings::Pin_Back_Boundary)
		// void Pin_Front_Boundary();
		.def ("Pin_Front_Boundary", &ExperimentSettings::Pin_Front_Boundary)
		// void Pin_Bottom_Boundary();
		.def ("Pin_Bottom_Boundary", &ExperimentSettings::Pin_Bottom_Boundary)
		// void Pin_Top_Boundary();
		.def ("Pin_Top_Boundary", &ExperimentSettings::Pin_Top_Boundary)
		// void Pin_Left_Boundary();
		.def ("Pin_Left_Boundary", &ExperimentSettings::Pin_Left_Boundary)
		// void Pin_Right_Boundary();
		.def ("Pin_Right_Boundary", &ExperimentSettings::Pin_Right_Boundary)
		// this pins an additional number of layers at the bottom of the model
		// nb = 0: only bottom-boundary, nb = 1: bottom-boundar + additional layer, nb = 2: ...
		// void Pin_Bottom_Boundary_Several_Layer(int nb)
		.def ("Pin_Bottom_Boundary_Several_Layer", &ExperimentSettings::Pin_Bottom_Boundary_Several_Layer)

		// this multiplies the youngs-modulus with a factor of the particles inbetween the given coordinates
		// void HardenParticles ( Real factor, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax );
		.def("HardenParticles", &ExperimentSettings::HardenParticles)

		// void RemoveSprings (Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax)
		// removes springs in a certain volume, making the material granular while the rest of the model remains a lattice
		.def ("RemoveSprings", &ExperimentSettings::RemoveSprings)

		/******************************
		* viscoelasticity
		*****************************/
		// void SetViscosity(Real viscosity, Real x1, Real x2, Real y1, Real y2, Real z1, Real z2);
		// default is 1e23 (upper crust viscosity)
		.def("SetViscosity", &ExperimentSettings::SetViscosity)
		// void SetTimeStep(Real time);
		// default is 0
		.def("SetTimeStep", &ExperimentSettings::SetTimeStep)
		// void EnableViscoelasticRelaxation();
		// default is false
		.def("ActivateLatticeViscoelasticity", &ExperimentSettings::ActivateLatticeViscoelasticity)
		// power law viscosity
		// void ExperimentSettings::SetPowerLawViscosity(Real minx , Real maxx , Real miny , Real maxy , Real minz , Real maxz, Real A, Real Ea, Real Va, Real n)
		.def("SetPowerLawViscosity", &ExperimentSettings::SetPowerLawViscosity, SetPowerLawViscosity_overloads()) 
		// sets the strength of second-neighbor springs to a multiple of the 1st-neighbour spring
		.def("ScaleSecNeigStrength", &ExperimentSettings::ScaleSecNeigStrength)            

		// failure mode of the springs: stress tensor based or spring extension based
		// void ExperimentSettings::Set_Tensor_Failure_Mode(bool stress_tensor_based_failure)
		.def("SetTensorFailureMode", &ExperimentSettings::Set_Tensor_Failure_Mode)

		// void ExperimentSettings::SetTemperature( Real minx , Real maxx , Real miny , Real maxy , Real minz , Real maxz, Real temperature);
		.def("SetTemperature", &ExperimentSettings::SetTemperature)

		// angle of internal friction for the mohr-coulomb criterion. Default is pi/6
		// void Set_Angle_Of_Internal_Friction(Real angle_of_internal_friction, Real xmin, Real xmax, Real ymin, Real ymax, Real zmin, Real zmax);
		.def("Set_Angle_Of_Internal_Friction", &ExperimentSettings::Set_Angle_Of_Internal_Friction)

		// void Activate_Mohr_Coulomb_Failure(bool activate);
		.def("Activate_Mohr_Coulomb_Failure", &ExperimentSettings::Activate_Mohr_Coulomb_Failure)
		;

    class_<Model>( "Model", init < >() )

		// overloaded function, can be called with (nb_of_particles_in_x, x_dim, y_dim, z_dim, use_periodic_boundaries)
		// further definition and boost-python-macro-calling in model.h
		// void CreateNewModel(int resolution_in_x = 50, Real xlength = 1.0, Real ylength = 1.0, Real zlength = 1.0, 
		// 		bool bool is_second_neighbour_model = true, bool repulsion = true,
		// 		bool second_neig_repulsion = true,  bool walls_in_x = false, bool walls_in_z = false, 
		// 		bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);
		.def("CreateNewModel", &Model::CreateNewModel)

		// dump a vtk-file
		// void DumpVTKFile(string stdpath);
		.def("DumpVTKFile", &Model::DumpVTKFile)

		.def("Info", &Model::Info)
		.def("Average_mean_stress", &Model::Average_mean_stress)
		;
}


/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RELAXATION_H
#define RELAXATION_H

#include "model.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//newmat (matrix-operations)
#include <iostream>
#include "../newmat10/newmatap.h"
#include <iomanip>
#include "../newmat10/newmatio.h"

#include <omp.h>

class Relaxation 
{ 
public:
    Relaxation(Model *mod);
    ~Relaxation();

    void Relax();
    void SetRelaxationThreshold(Real relaxthreshold);
    
	// sets the factor for the overrelaxation. Default value (set in constructor of relaxation-class) is 1.8
	void SetOverRelaxationFactor(Real fover);
    void ActivateDynamicORF(bool activate);

    void Info();	// output some settings

protected:
private:
    bool RelaxModel(Particle *prtcl, int threadnum);
    inline void UpdateParticlePos(Particle *prtcl);
    
    void ViscousFlow();

    Real GetCurrentNewtonianViscosity(Particle *prtcl);
    
    bool CalcStressAndBrittleFailure(Particle *prtcl, int threadnum);
    bool FullRelax();
    
    Real fover;

    bool dynamic_relax_thresh;

    struct RemoveParticleFromList
    {
		bool operator()(Particle::visc_unconnected_particle tmp)
		{
			return (tmp.remove);
		};
	};

    Model *model;
    Real relaxthreshold;
};

#endif // RELAXATION_H

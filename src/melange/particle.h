/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PARTICLE_H
#define PARTICLE_H

#include <math.h>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <list>

#include <omp.h>

//newmat (matrix-operations)
#include "../newmat10/newmatap.h"
#include <iomanip>
#include "../newmat10/newmatio.h"

#include "global_defs"

using namespace globaldefs;
//using namespace NEWMAT;
using namespace std;

// newmat uses the typedef Real, thus we can define the precision here. It makes sense to leave it at Real 
typedef Real Real;

class Particle
{
public:
    Particle();
    ~Particle();

    // common geometric particle properties
    m3dVector pos;
    Real radius;
    Real second_neig_radius;
    
    m3dVector neigpos[12];
    m3dVector secneigpos[6];

    // the following block of variables is used in the viscous flow routine only
    m3dVector neig_hull_intersect[12];		// this is the intersection of the vector to a neighbour-spring with the particle hull due to viscous flow
    m3dVector sec_neig_hull_intersect[6];		// this is the intersection of the vector to a second-neighbour-spring with the particle hull due to viscous flow

    // the viscous transformation Matrix. It is initiallize as identity matrix, then multiplicated with sub-transformation matrices
    Matrix T;
    
    Real br_strength;
    
    // 'backpointer': gives the spring number which is connected to the neighbour
    int neig_back_ptr[12];
    int sec_neig_back_ptr[6];

    // intermediately stores the new x/y/z-pos for parallelization
    Real xx, yy, zz;
    
   	Real xlayer, ylayer, zlayer;

    // system boundaries
    bool is_boundary;
        
    // general system info
    // most (but not all: see the repulsion) of the
    Real wrapping_constant_x, wrapping_constant_z;
    bool use_periodic_boundaries_in_x, use_periodic_boundaries_in_z;
    Real scale_factor;
    Real use_gravity_force;
   	bool repulsion;									// use repulsion? handling of variable on particle basis different from the model-variable
    bool is_second_neighbour_model;					// are u a second-neighbour model? or only repulsion?

    bool walls_in_x;
    bool walls_in_z;
    void InitModelFlags( bool repulsion, bool is_second_neighbour_model, bool walls_in_x, bool walls_in_z);
    
    // repulsion-box
	int repbox_dim_y, repbox_dim_x, repbox_dim_z;
	
	// model dimensions
	Real xlength, ylength, zlength;
	
    bool is_top_boundary, is_bottom_boundary, is_left_boundary;
    bool is_right_boundary, is_front_boundary, is_back_boundary;

    // domain boundaries (e.g. of grains)
    bool is_domain_boundary;

    bool is_broken;
    bool second_neighbour_is_broken;

    // tetahedra
    m3dVector tetrahedron_edges[12];
    m3dVector tetrahedron_normals[12];

    // stores the direct particle-neighbours
    Particle *neighbour_list[12];
    Particle *second_neighbour_list[6];

    // mechanical parameters / helper variables for the breaking routine / material parameters
    Real break_Str[12];
    Real second_neigh_break_Str[6];
    bool no_break[12];
    bool second_neigh_no_break[6];
    Real mbreak;
    int neigh;
    bool fix_x, fix_y, fix_z;
    Real fover;
    Real relaxthreshold;
    bool dynamic_relax_thresh;
    
	Real young;
	Real v;	// poisson number
	Real k1; 	// the spring constant
	Real k2; 	// 2nd neighbour spring constant
	Real ks1; 	// shear spring constant
	Real ks2; 	// 2nd neighbour shear spring constant
    Real volume;

    // temperature
    Real temperature;

    // angle of internal friction (mohr-coulomb criterion)
    Real angle_of_internal_friction;

    // spring failure: tensor controlled or spring extension controled?
    bool stress_tensor_based_failure;

    // natural material parameters
    Real density;
    Real real_young;
    Real standard_real_young;
    Real viscosity;			// default is 1e23 (upper crust viscosity)
	
    // power law viscosity
    bool power_law_viscosity;
    Real A, Ea, Va, n;            // pre-factor, activation energy, activation volume, exponent (of power law viscosity)

	// displacement due to gravity
	Real fg;
	
	/*
	 * system is viscoelastic. default = false
	 */
	bool viscoelastic;
	
    /* *
     * dynamic list of unconnected neighbour-particles
     * */
	// the unconnected neighbours
	struct visc_unconnected_particle
	{
		Particle *neig;
		Real alen;
		
		bool remove;
	};
	list<visc_unconnected_particle> visc_unconnected_neigh_list;
	
    void BreakBond();
    void WeakenBond(Real young_factor, Real break_factor);

    // indivual nb of this particle (identical to the list-position)
    int nb;

    // triangles of the tetrahedron
    void SetGeometry(Real rad, Real second_rad);
    
    void SetYoungsModulusAndV(Real real_young, Real v);

    // the repulsion box
    int box_pos[3];
    Particle *next_in_box;
    bool done;
	bool *done_for_multithreads;

    // stresses in 3D
    /* Tensor:
     * 	sxx 	sxy 	sxz
     * 	sxy		syy		syz
     * 	sxz		syz		szz
     */
    Real sxx, syy, szz, sxy, sxz, syz;
    Real sigma1, sigma2, sigma3;
    Real sigma1_vec[3], sigma2_vec[3], sigma3_vec[3];
    Real mean_stress, diff_stress;
    
    Real GetXDist( Particle *curpar );
    Real GetZDist( Particle *curpar );

    Real GetRepAlen(Particle *neig);

    // Coefficients for the description of the ellipsoid-shape
    Real e11, e21, e22, e31, e32, e33;

protected:

private:

};

#endif // PARTICLE_H

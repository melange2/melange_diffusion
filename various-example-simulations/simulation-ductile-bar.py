#!/usr/bin/python

import os, time
from math import *
from melange import *
from pylab import *

workingdir = os.getcwd()

m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

## CreateNewModel(int resolution_in_x = 50, float xlength = 1.0, float ylength = 1.0, float zlength = 1.0,
m.CreateNewModel( 40, 1.0, 0.3, 0.3,
True, True, 		# bool is_second_neighbour_model = true, bool repulsion = true,
False, False, 		# bool walls_in_x = false, bool walls_in_z = false,
False, False )		# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

r.SetRelaxationThreshold(1e-8)

r.SetOverRelaxationFactor(1.1)

e.SetStandardYoungsModulusAndV(200e9, 0.24)

e.SetDensityForAllParticles(2600)

e.SetScalingFactor(1)

e.SetViscosity( 1e10, 0.0, 10.0, 0.0, 10.0, 0.0, 10.0 )
e.SetViscosity( 1e30, 0.0, 0.3, 0.0, 10.0, 0.0, 10.0 )
e.SetViscosity( 1e30, 0.7, 1.0, 0.0, 10.0, 0.0, 10.0 )

e.Set_Elastic_Viscous_Threshold(1e27)

# e.EnableSideWallBreakingInZAndX() # this does ALSO enable breaking of top-particles
# e.EnableSideWallBreakingInYAndZ()
e.EnableSideWallBreakingEverywhere()
e.Unfix_z()
e.Unfix_y()
# e.Unfix_x()

e.ChangeBreakingStrength ( -500e7, -10.0, 10.0, -10.0, 10.0, -10.0, 10.0 )

e.Drucker_Prager_Failure(False)
e.Standard_Spring_Failure(True)

# default = 1.0
e.Fracture_Probability_Scale(1.0)

# default: true
e.Tensile_Failure(True)
# default: true
e.Shear_Failure(True)

# default = 1.0
e.Shear_Cohesion_Scale(1.0)
# default = 1.0
e.Tensile_Strength_Scale(1.2)

# default: False
e.Griffith_Failure_Criterion(True)

e.Set_Angle_Of_Internal_Friction(pi/5.2, 0.0, 10.0, 0.0, 1.0, 0.0, 10.0)

e.ActivateLatticeViscoelasticity()

# output and summary in cli
m.Info()
r.Info()

m.DumpVTKFile("original")

e.ActivateSpringBreaking(False)

# default: True. Breaking strength is adapted to the shape of particles. thin shape: small breaking strength in thinned direction.
e.AdaptBreakingStrength(True)

e.SetTimeStep (600000.0)					# 5000 a / 10m extension

# initial relaxation (or right after the viscoelastic equilibrium is achieved)
r.Relax()
m.DumpVTKFile("graben_0")

e.ActivateSpringBreaking(True)

"""
deformation, relaxation, output
"""

dx = 0.01
d.DeformLattice (dx)
	
for t in range ( 1, 100001, 1 ):

	# deformation

	dx = 0.01
	d.DeformLattice (dx)
	start = time.time()

	r.Relax()

	print '\nRelaxation took: ' + str(time.time() - start) + '\nEnd of timestep ' + str(t) + '\n\n'

	# if t % 1 == 0:
	m.DumpVTKFile("steel"+`t`)

print "end"

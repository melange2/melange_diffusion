#!/usr/bin/python 

"""
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, time
from melange import *

workingdir = os.getcwd()

"""
create necessary objects
"""
m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

"""
initialize
"""
m.CreateNewModel( 50,	# int resolution_in_x = 50, 
1, 0.175, 0.1, 		# double xlength = 1.0, double ylength = 1.0, double zlength = 1.0, 
True, True, 		# bool is_next_neighbour_model = true, bool is_second_neighbour_model = true, 
True, 			# bool repulsion = true, 
False, True, 		# bool walls_in_x = false, bool walls_in_z = false, 
False, True )		# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

r.SetRelaxationThreshold(1e-9)
## good for granular materials
#r.SetOverRelaxationFactor(2.044)
## good for lattices:
r.SetOverRelaxationFactor(1.90)

e.SetStandardKandYoungsModulus(100e9);

e.SetRealKandYoungsModulus(100e9, 0.25, 0.75, 0.0, 0.025, 0.0, 1.0)
#e.SetRealKandYoungsModulus(200e9, 0.0, 0.25, 0.0, 0.025, 0.0, 1.0)
#e.SetRealKandYoungsModulus(200e9, 0.75, 1.0, 0.0, 0.025, 0.0, 1.0)

'''
General (real) physical parameters
'''
## DEFAULT for the density is 2700 kg/m**3
e.SetDensityForAllParticles(2700)               # upper crust density
## DEFAULT for the systemsize is 100 km
e.SetScalingFactor(20000)                      # 20 km



'''
viscoelasticity
'''
# sets the viscosity for a box of particles. box given bei x1/x2, y1/y2, z1/z2
# void SetViscosity(double viscosity, double x1, double x2, double y1, double y2, double z1, double z2);
# default for the viscosity is 1e23, the value for the upper crust
e.SetViscosity( 1e27, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0 )
#~ e.SetViscosity( 5e24, 0.0, 1.0, 0.6, 0.7, 0.0, 1.0 )
#~ e.SetViscosity( 1e24, 0.0, 1.0, 0.5, 0.6, 0.0, 1.0 )
#~ e.SetViscosity( 5e23, 0.0, 1.0, 0.35, 0.5, 0.0, 1.0 )
e.SetViscosity( 1e14, 0.25, 0.75, 0.0, 0.025, 0.0, 1.0 )

# seconds; timestep is set to 0 bei default
e.SetTimeStep(864000000.0)	# 100.000 a

"""
Control over the boundaries,
whether they are fixed, are 
allowed to break, etc.
"""
e.EnableSideWallBreakingInZ()
#e.EnableSideWallBreakingInZAndX()
#e.Unfix_z()
#e.Unfix_x()

e.SetGaussianStrengthDistribution(1.0, 0.0005, 0, 1.0, 0, 1.0, 0, 1.0)
#e.RemoveSprings(-10.0, 10.0, 0.025, 10.0, -10.0, 10.0)
e.ChangeBreakingStrength( 0.002, -10.0, 10.0, 0.025, 0.4, -10.0, 10.0 )
e.ChangeBreakingStrength( 1.0, -10.0, 10.0, 0.0, 0.025, -10.0, 10.0 )

#e.Pin_Bottom_Boundary_Several_Layer(1)
e.Pin_Bottom_Boundary()
e.ActivateGravity()
## use viscoelasticity (i.e.: change the young's modulus for most particles accordingly)
e.ActivateLatticeViscoelasticity()

e.SetNoBreakY (0.025)

# 
e.ScaleSecondNextToNextNeighbourBrStr()
# for further scaling, if 2nd-neig springs still localize to much
#e.MultiplySecNeigStrength(0.7)

### output and summary in cli
m.Info()
r.Info()

start = time.time()
r.Relax()
print("Initial relaxation took:", time.time() - start)
m.DumpVTKFile("res-"+`0`)

for t in range (1, 100001, 1):

        #d.DeformLatticeSandbox(0.0000025, 0.00625, 0.48, 0.52)
        d.DeformLatticeSandbox(0.0000025, 0.01, 0.48, 0.52)

        start = time.time()

        r.Relax()

        print("Relaxation took:", time.time() - start)

	if t % 1000 == 0:
	        m.DumpVTKFile("res-"+`t`)

print "end"


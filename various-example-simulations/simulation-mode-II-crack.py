#!/usr/bin/python 

"""
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, time
from melange import *

workingdir = os.getcwd()

"""
create necessary objects
"""
m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

"""
initialize
"""
## CreateNewModel(int resolution_in_x = 50, double xlength = 1.0, double ylength = 1.0, double zlength = 1.0,
m.CreateNewModel( 25, 1.0, 1.0, 1.0, 
True, True, True, 	# bool is_next_neighbour_model = true, bool is_second_neighbour_model = true, bool repulsion = true, 
False, False, 		# bool walls_in_x = false, bool walls_in_z = false, 
False, True )		# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

r.SetRelaxationThreshold(1e-8)
r.SetOverRelaxationFactor(2.04)

e.SetStandardKandYoungsModulus(1e9);

# mode 1 crack
e.SeverSpringsCrossingPlane( 0.5, 0.0, 0.5, 	0.5, 1.0, 0.0, 	0.5, 1.0, 1.0, 	0.0, 1.0, 0.0, 1.0, 0.2, 0.8);
# mode 2 crack

# multiplies the Youngs modulus of springs with random value (with mean +/- standard-deviation)
#e.SetRealKandYoungsModulus(100e9, 0.25, 0.75, 0.0, 0.025, 0.0, 1.0)
#e.SetRealKandYoungsModulus(200e9, 0.0, 0.25, 0.0, 0.025, 0.0, 1.0)
#e.SetRealKandYoungsModulus(200e9, 0.75, 1.0, 0.0, 0.025, 0.0, 1.0)

"""
General (real) physical parameters
"""
## DEFAULT for the density is 2700 kg/m**3
e.SetDensityForAllParticles(2700)               # upper crust density
## DEFAULT for the systemsize is 100 km
e.SetScalingFactor(20000)                      # 20 km



"""
viscoelasticity
"""
# sets the viscosity for a box of particles. box given bei x1/x2, y1/y2, z1/z2
# void SetViscosity(double viscosity, double x1, double x2, double y1, double y2, double z1, double z2);
# default for the viscosity is 1e23, the value for the upper crust
#e.SetViscosity( 1e27, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0 )
#~ e.SetViscosity( 5e24, 0.0, 1.0, 0.6, 0.7, 0.0, 1.0 )
#~ e.SetViscosity( 1e24, 0.0, 1.0, 0.5, 0.6, 0.0, 1.0 )
#~ e.SetViscosity( 5e23, 0.0, 1.0, 0.35, 0.5, 0.0, 1.0 )
#e.SetViscosity( 1e14, 0.25, 0.75, 0.0, 0.025, 0.0, 1.0 )

# seconds; timestep is set to 0 bei default
e.SetTimeStep(86400000000.0)	# 1 Ma

"""
Control over the boundaries,
whether they are fixed, are 
allowed to break, etc.
"""
#e.EnableSideWallBreakingInZ()
#e.EnableSideWallBreakingInZAndX()
e.Unfix_z()
#e.Unfix_x()

#e.RemoveSprings(-10.0, 10.0, 0.025, 10.0, -10.0, 10.0)
#e.ChangeBreakingStrength( 0.001, -10.0, 10.0, 0.1, 0.4, -10.0, 10.0 )
#e.ChangeBreakingStrength( 1.0, -10.0, 10.0, 0.0, 0.025, -10.0, 10.0 )

#e.Pin_Bottom_Boundary_Several_Layer(1)
#e.Pin_Bottom_Boundary()
#e.ActivateGravity()
## use viscoelasticity (i.e.: change the young's modulus for most particles accordingly)
#e.ActivateLatticeViscoelasticity()

e.SetNoBreakY (1.0)

e.ScaleSecondNextToNextNeighbourBrStr()

### output and summary in cli
m.Info()
r.Info()

start = time.time()
r.Relax()
print("Initial relaxation took:", time.time() - start)
m.DumpVTKFile("res-"+`0`)

for t in range (1, 10, 1):

        d.DeformLatticeSimpleShear(0.001)

        start = time.time()

        r.Relax()

        print("Relaxation took:", time.time() - start)

        m.DumpVTKFile("res-"+`t`)

print "end"


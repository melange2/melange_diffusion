# Repo for the 'Melange' Discrete Element elasto-visco-plastic code 

This is the new repository dedicated to hast Melange stable releases.

'Melange' is a 2D / 3D lattice-particle hybrid model. The software is designed in order to simulate ductile visco-elasto-plastic deformation and can be used to model deformation processes on all scales. The code takes the most relevant yield mechanisms for the deformation of geo-materials into account: dynamic brittle failure and ductile creep, where ductile creep is modeled as viscoelasticity.

![Alt text](Extensions-Graben.png)

*Example application: development of a graben under extension. Includes dynamic fracture formation, subsidence and tilting of blocks.*  

### Relevant publication

Sachau, T. and Koehn, D. „A new mixed-mode fracture criterion for large-scale lattice models“. Geoscientific Model Development 7, Nr. 1 (Januar 2014): 243–47. https://doi.org/10.5194/gmd-7-243-2014.

Sachau, T. and Koehn, D. „‚Melange‘: A viscoelastic lattice-particle model applicable to the lithosphere“. Geochemistry Geophysics Geosystems 13, Nr. 1 (2012). https://doi.org/10.1029/2012GC004452.

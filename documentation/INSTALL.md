The easiest way to install 'Melange' is to install the deb file which we provide on the launchpad site. Currently, the deb-file works on Ubuntu 12.10 and probably on Ubuntu 12.04.

If installation from the deb-file works for you -- its as simple as downloading and double-clicking --, you can skip point *I - III* and proceed with the further documentation (*IV - Where to go from here*).

If you can not install from the deb-file, you have to compile and install from source. Follow steps *I - III* for this purpose. 

Note that this explanation has been written with Debian and Ubuntu in mind. If you want to use a different Liux distribution, compiation is straightforward if you know how to install the dependencies (see *I*) on your system. The rest should be similar.

#### I) Install depencies and build system on Ubuntu:

open a terminal, and type (without quotes):

	sudo apt-get install libnewmat10-dev libboost-python libgomp1 scons paraview

besides the dependencies, this will also download the package 'Paraview', which can be used to vissualize the results.

#### II) Download sources and compile
	
now download the melange-x.y.tar.gz archive and unzip it. unzipping is done by either rightclicking the archive and selecting 'unzip' or an similar entry in the popup menu, or by opening a terminal, navigating to the parent folder and typing 

	tar -xf melange-x.y.tar.gz

to compile 'Melange', open a terminal in the melange directory (the one which contains the file SConstruct). now simply type:

	scons

This command should compile and link the source code, resulting in the shared library file 'melange.os' in the same directory.
	
#### III) Install 

the file 'melange.os' has to be imported into a python script, from where its functionality can be accessed. for this purpose the Python-interpreter has to find it, which means we have to copy it to a known directory.

there are 2 ways to achieve this:

1. the easiest way to install it globally is to copy it to '/usr/lib/pymodules/python2.7' by:

	sudo cp melange.so /usr/lib/pymodules/python2.7/.

2. alternatively you can just copy the 'melange.so' to your current working directory. In this case don't forget: since it is not globally installed you have to copy it everytime you want to run a simulation to the directory where you are running the simulation script!

#### IV) Where to go from here?

Have a close look at the provided documentation:

* You might want to have a look at the tutorial, which gives a short step-by-step walkthrough how to run your first simulation from one of the provided simulation scripts.
* It is probably also useful to have a look at the 'Using Melange' file, which gives some extra information and also describes some of the software tools that you will need in order to set up your own simulations.
* Finally, it makes a lot of sense to have a close look at the documented simulation file. In fact: once you have 'Melange' up and running, this is probably the single most important file in order to learn how to use it and to run simulations.